package main

import (
	"testing"

	"gitlab.com/chelmertz/adventofcode/2022/aoc"
)

func Test_a_fixture(t *testing.T) {
	test :=
		`    [D]    
[N] [C]    
[Z] [M] [P]
 1   2   3 

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2`

	wanted := "CMZ"
	if actual := answerA(test); wanted != actual {
		t.Errorf("got %v, wanted %v", actual, wanted)
	}
}

func Test_a_input(t *testing.T) {
	real := aoc.InputTxt()
	wanted := "VRWBSFZWM"
	if actual := answerA(real); wanted != actual {
		t.Errorf("got %v, wanted %v", actual, wanted)
	}
}

func Test_b_fixture(t *testing.T) {
	test1 :=
		`    [D]    
[N] [C]    
[Z] [M] [P]
 1   2   3 

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2`

	wanted1 := "MCD"
	if actual1 := answerB(test1); wanted1 != actual1 {
		t.Errorf("got %v, wanted %v", actual1, wanted1)
	}
}

func Test_b_input(t *testing.T) {
	test1 := aoc.InputTxt()
	wanted1 := "RBTWJWMCF"
	if actual1 := answerB(test1); wanted1 != actual1 {
		t.Errorf("got %v, wanted %v", actual1, wanted1)
	}
}
