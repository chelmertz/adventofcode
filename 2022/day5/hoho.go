package main

import (
	"bufio"
	"strings"

	"gitlab.com/chelmertz/adventofcode/2022/aoc"
)

const (
	readingBoxes = iota
	readingMoves
)

var cleanDirections = strings.NewReplacer(
	"move", "",
	"from", "",
	"to", "",
)

// end of string = top of stack
type prog []string

func top(c string, n int) string {
	return string(c[len(c)-n:])
}

func shift(c string, n int) string {
	return c[0 : len(c)-n]
}

func word(p prog) string {
	w := ""
	for _, s := range p {
		if len(s) > 0 {
			w += top(s, 1)
		} else {
			w += " "
		}
	}
	// trim, we might have used more columns than necessary
	return strings.TrimSpace(w)
}

func answerA(text string) string {
	mode := readingBoxes
	scanner := bufio.NewScanner(strings.NewReader(text))
	// nine cols at most
	stacks := make(prog, 10)

	for scanner.Scan() {
		line := scanner.Text()
		if mode == readingBoxes && line[1] == '1' {
			mode = readingMoves
			continue
		}
		if strings.TrimSpace(line) == "" {
			continue
		}

		aoc.AssertTrue(mode == readingBoxes || mode == readingMoves)

		if mode == readingBoxes {
			// instructions are 1-indexed
			// interesting x: 1, 5, 9, ..
			for i, col := 1, 1; i < len(line); i, col = i+4, col+1 {
				possibleLetter := line[i]
				if possibleLetter != ' ' {
					stacks[col] = string(possibleLetter) + stacks[col]
				}
			}
		} else if mode == readingMoves {
			dirs := cleanDirections.Replace(line)
			cleanDirs := strings.Fields(dirs)
			count, from, to := aoc.Atoi(cleanDirs[0]), aoc.Atoi(cleanDirs[1]), aoc.Atoi(cleanDirs[2])

			aoc.AssertTrue(from >= 1)
			aoc.AssertTrue(to >= 1)
			fromLen := len(stacks[from])
			toLen := len(stacks[to])
			for i := 0; i < count; i++ {
				aoc.AssertTrue(len(stacks[from]) > 0)
				stacks[to] = stacks[to] + top(stacks[from], 1)
				stacks[from] = shift(stacks[from], 1)

			}
			aoc.AssertTrue(len(stacks[from]) == fromLen-count)
			aoc.AssertTrue(len(stacks[to]) == toLen+count)
		}
	}
	return word(stacks)
}

func answerB(text string) string {
	mode := readingBoxes
	scanner := bufio.NewScanner(strings.NewReader(text))
	// nine cols at most
	stacks := make(prog, 10)

	for scanner.Scan() {
		line := scanner.Text()
		if mode == readingBoxes && line[1] == '1' {
			mode = readingMoves
			continue
		}
		if strings.TrimSpace(line) == "" {
			continue
		}

		aoc.AssertTrue(mode == readingBoxes || mode == readingMoves)

		if mode == readingBoxes {
			// instructions are 1-indexed
			// interesting x: 1, 5, 9, ..
			for i, col := 1, 1; i < len(line); i, col = i+4, col+1 {
				possibleLetter := line[i]
				if possibleLetter != ' ' {
					stacks[col] = string(possibleLetter) + stacks[col]
				}
			}
		} else if mode == readingMoves {
			dirs := cleanDirections.Replace(line)
			cleanDirs := strings.Fields(dirs)
			count, from, to := aoc.Atoi(cleanDirs[0]), aoc.Atoi(cleanDirs[1]), aoc.Atoi(cleanDirs[2])

			aoc.AssertTrue(from >= 1)
			aoc.AssertTrue(to >= 1)
			fromLen := len(stacks[from])
			toLen := len(stacks[to])
			aoc.AssertTrue(len(stacks[from]) >= count)

			stacks[to] = stacks[to] + top(stacks[from], count)
			stacks[from] = shift(stacks[from], count)

			aoc.AssertTrue(len(stacks[from]) == fromLen-count)
			aoc.AssertTrue(len(stacks[to]) == toLen+count)
		}
	}
	return word(stacks)
}
