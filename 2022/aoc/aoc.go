package aoc

import (
	"os"
	"sort"
	"strconv"
)

func AssertTrue(st bool) {
	if !st {
		panic("false")
	}
}

func AssertNotErr(err error) {
	if err != nil {
		panic(err)
	}
}

func AssertNotZero[T comparable](s T) {
	if s == *new(T) {
		panic("empty")
	}
}

func AssertNotEmpty[T any](s []T) {
	if len(s) == 0 {
		panic("empty")
	}
}

func Sum(s []int) int {
	total := 0
	for _, i := range s {
		total += i
	}
	return total
}

func Max(ints []int) int {
	AssertNotEmpty(ints)
	sort.Ints(ints)
	return ints[len(ints)-1]
}

func Atoi(s string) int {
	result, err := strconv.Atoi(s)
	AssertNotErr(err)
	return result
}

type AocSet[T comparable] struct {
	items map[T]struct{}
}

func NewSet[T comparable]() *AocSet[T] {
	set := new(AocSet[T])
	set.items = make(map[T]struct{})
	return set
}

func NewRuneSet(chars string) *AocSet[rune] {
	set := NewSet[rune]()
	for _, c := range chars {
		set.items[c] = struct{}{}
	}
	return set
}

func (s *AocSet[T]) Intersect(other *AocSet[T]) *AocSet[T] {
	result := NewSet[T]()
	for k := range s.items {
		if _, ok := other.items[k]; ok {
			result.items[k] = struct{}{}
		}
	}
	return result
}

func (s *AocSet[T]) Len() int {
	return len(s.items)
}

func (s *AocSet[T]) Each(cb func(item T) bool) {
	for k := range s.items {
		if keepGoing := cb(k); !keepGoing {
			break
		}
	}
}

func InputTxt() string {
	text, err := os.ReadFile("input.txt")
	AssertNotErr(err)

	return string(text)
}
