package main

import (
	"fmt"
	"os"
	"strings"
)

/*
/a/b/ 234j234 // total sum of this dir, either recursively or just directly. if "just directly", traversal would need to visit all dirs
/a/b/j 234234 // a file's size (probably useful for part 2)

// screw it, let's just track files and do a common prefix search thingy
*/

var sample = `$ cd /
$ ls
dir a
14848514 b.txt
8504156 c.dat
dir d
$ cd a
$ ls
dir e
29116 f
2557 g
62596 h.lst
$ cd e
$ ls
584 i
$ cd ..
$ cd ..
$ cd d
$ ls
4060174 j
8033020 d.log
5626152 d.ext
7214296 k`

// map string filesize
// new pass over all files, sum size for each dir, map dir filesize (each file adds to all dirs above)
// new pass, find largest dir below threshold

func cd(cwd, target string) string {
	if target[0] == '/' {
		// absolute path
		return target
	}
	cwdSplit := strings.Split(strings.Trim(cwd, "/"), "/")
	if target == ".." {
		cwdSplit = cwdSplit[:len(cwdSplit)-1]
	} else if len(cwdSplit) == 1 && cwdSplit[0] == "" {
		// strings.Split()'s behavior is weird, mitigate it
		cwdSplit = []string{target}
	} else {
		cwdSplit = append(cwdSplit, target)
	}
	return fmt.Sprintf("/%s/", strings.Trim(strings.Join(cwdSplit, "/"), "/"))
}

func main() {
	// absolute paths
	fileSizes := make(map[string]int)
	recursiveDirSizes := make(map[string]int)

	// only two commands: cd,ls, making the state very simple. ls doesn't even take a path argument, cd only takes "/", ".." or a single dir
	var cwd, cdDir, lsEntry string
	var fileSize int

	// collect fs info
	input, err := os.ReadFile("input.txt")
	if err != nil {
		panic(err)
	}
	for _, l := range strings.Split(string(input), "\n") {
		// cd
		_, err := fmt.Sscanf(l, "$ cd %s", &cdDir)
		if err == nil {
			cwd = cd(cwd, cdDir)
			continue
		}

		// ls
		_, err = fmt.Sscanf(l, "$ ls %s", &cdDir)
		if err == nil {
			continue
		}

		// ls output, dir
		_, err = fmt.Sscanf(l, "dir %s", &lsEntry)
		if err == nil {
			continue
		}

		// ls output, file
		_, err = fmt.Sscanf(l, "%d %s", &fileSize, &lsEntry)
		if err == nil {
			fileSizes[fmt.Sprintf("%s%s", cwd, lsEntry)] = fileSize
			// not very optimized :P
			path := ""
			paths := strings.Split(strings.Trim(cwd, "/"), "/")
			if len(paths) != 1 || paths[0] != "" {
				// I'm still not very fond of strings.Split()'s behavior
				recursiveDirSizes["/"] += fileSize
			}
			for _, dir := range paths {
				path += "/" + dir
				recursiveDirSizes[path] += fileSize
			}
			continue
		}
	}

	var sumOfLte100k int
	for _, size := range recursiveDirSizes {
		if size <= 100_000 {
			sumOfLte100k += size
		}
	}
	fmt.Println(sumOfLte100k)

	// allowed to use = 70-30=40
	// must delete = totalSize-40

	totalDisk := 70_000_000
	neededForUpgrade := 30_000_000
	allowedToUse := totalDisk - neededForUpgrade
	mustDelete := recursiveDirSizes["/"] - allowedToUse

	dirToRemovesSize := 0
	for _, size := range recursiveDirSizes {
		if (dirToRemovesSize == 0 || size <= dirToRemovesSize) && size >= mustDelete {
			dirToRemovesSize = size
		}
	}
	fmt.Println(dirToRemovesSize)

}
