package main

import (
	"fmt"
	"strings"

	"gitlab.com/chelmertz/adventofcode/2022/aoc"
)

var pickPoints = map[rune]int{
	'A': 1,
	'B': 2,
	'C': 3,
}

const (
	draw = 3
	win  = 6
)

func calc(them, us rune) int {
	aoc.AssertTrue(us == 'A' || us == 'B' || us == 'C')
	aoc.AssertTrue(them == 'A' || them == 'B' || them == 'C')
	point := pickPoints[us]
	aoc.AssertTrue(point > 0)
	if them == us {
		point += draw
	} else if them-us == 1 || (them == 'A' && us == 'C') {
		// loss (|| wrap around)
	} else {
		// win (|| wrap around)
		point += win
	}
	//fmt.Printf("point: %d for setup: them=%c, us=%c\n", point, them, us)
	return point
}

func p1normalize(_, us rune) rune {
	return us - 23
}

func p2normalize(them, us rune) rune {
	switch us {
	case 'X':
		// must lose
		switch them {
		case 'A':
			return 'C'
		case 'B':
			return 'A'
		case 'C':
			return 'B'
		}
		return them
	case 'Y':
		// draw
		return them
	case 'Z':
		// must win
		switch them {
		case 'A':
			return 'B'
		case 'B':
			return 'C'
		case 'C':
			return 'A'
		}
	}
	panic("bad 'us' value")
}

func answer(text string, pick func(them, us rune) rune) int {
	score := 0
	for _, play := range strings.Split(text, "\n") {
		var them, us rune
		_, err := fmt.Sscanf(play, "%c %c", &them, &us)
		us = pick(them, us)
		//fmt.Println(play, them, us)
		aoc.AssertNotErr(err)
		score += calc(them, us)
	}
	aoc.AssertTrue(score > 0)
	return score
}

func main() {
	test := `A Y
B X
C Z`

	fmt.Println(answer(test, p1normalize))
	real := aoc.InputTxt()
	fmt.Println(answer(real, p2normalize))
}
