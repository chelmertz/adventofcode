package main

import (
	"fmt"
	"sort"
	"strconv"
	"strings"

	"gitlab.com/chelmertz/adventofcode/2022/aoc"
)

func answer(text string) (int, int) {
	max := make([]int, 0)
	for _, elf := range strings.Split(text, "\n\n") {
		elfKcal := 0
		kcals := strings.Split(elf, "\n")
		for _, kk := range kcals {
			kcal, err := strconv.Atoi(kk)
			aoc.AssertNotErr(err)
			elfKcal += kcal
		}
		max = append(max, elfKcal)
	}
	sort.Ints(max)
	return max[len(max)-1], aoc.Sum(max[len(max)-3:])
}

func main() {
	test := `1000
2000
3000

4000

5000
6000

7000
8000
9000

10000`

	real := aoc.InputTxt()
	fmt.Println(answer(test))
	fmt.Println(answer(real))
}
