package main

import (
	"fmt"
	"os"
	"path/filepath"
	"reflect"
	"strings"

	"gitlab.com/chelmertz/adventofcode/2022/aoc"
)

func main() {
	files, err := os.ReadDir(".")
	aoc.AssertNotErr(err)
	days := make([]int, 0)
	for _, f := range files {
		if name := f.Name(); f.Type().IsRegular() && name[0:3] == "day" {
			days = append(days, aoc.Atoi(strings.TrimSuffix(name[3:], filepath.Ext(name))))
		}
	}
	max := aoc.Max(days)
	fmt.Println(max)
	reflect.valueof()
	fun := reflect.ValueOf(fmt.Sprintf("day%d", max))
	aoc.AssertTrue(fun.Type().Kind() == reflect.Func)
	fun.Call(nil)

}
