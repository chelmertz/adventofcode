package main

import (
	"fmt"
	"strings"

	"gitlab.com/chelmertz/adventofcode/2022/aoc"
)

func answerA(text string) int {
	score := 0

	for _, line := range strings.Split(text, "\n") {
		var amin, amax, bmin, bmax int
		fmt.Sscanf(line, "%d-%d,%d-%d", &amin, &amax, &bmin, &bmax)
		aoc.AssertTrue(amin <= amax)
		aoc.AssertTrue(bmin <= bmax)
		if amin <= bmin && amax >= bmax {
			score++
		} else if bmin <= amin && bmax >= amax {
			score++
		}
	}
	aoc.AssertTrue(score > 0)
	return score
}

func answerB(text string) int {
	score := 0

	for _, line := range strings.Split(text, "\n") {
		var amin, amax, bmin, bmax int
		fmt.Sscanf(line, "%d-%d,%d-%d", &amin, &amax, &bmin, &bmax)
		aoc.AssertTrue(amin <= amax)
		aoc.AssertTrue(bmin <= bmax)
		if amin <= bmin && amax >= bmin {
			// a contains b
			score++
		} else if bmin <= amin && bmax >= amin {
			// b contains a
			score++
		}
	}
	aoc.AssertTrue(score > 0)
	return score
}
