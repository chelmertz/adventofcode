package main

import (
	"fmt"
	"testing"

	"gitlab.com/chelmertz/adventofcode/2022/aoc"
)

func Test_a_fixture(t *testing.T) {
	test := `2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8`

	wanted := 2
	if actual := answerA(test); wanted != actual {
		t.Errorf("got %v, wanted %v", actual, wanted)
	}
}

func Test_a_input(t *testing.T) {
	real := aoc.InputTxt()
	wanted := 550
	if actual := answerA(real); wanted != actual {
		t.Errorf("got %v, wanted %v", actual, wanted)
	}
}

func Test_b_fixture(t *testing.T) {
	test1 := `2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8`

	wanted1 := 4
	if actual1 := answerB(test1); wanted1 != actual1 {
		t.Errorf("got %v, wanted %v", actual1, wanted1)
	}
}

func Test_b_input(t *testing.T) {
	real := aoc.InputTxt()
	fmt.Println(answerB(real))
}
