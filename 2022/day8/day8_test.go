package main

import (
	"testing"
)

//func xTestBadSample(t *testing.T) {
//	var sample = `321304645532064452197673316923724578983475574649393989787463485775923766996461422676353215802454115
//233105015700144131749152169592849597799334664843639757588555464399727868723189114371617811204424461
//200116461554525361612654498296559659356483355845588365383658756592935783639172813284867400343573434`
//	if got := solve(sample); got != -1 {
//		t.Errorf("wanted something, got %d", got)
//	}
//}

func TestX(t *testing.T) {
	for _, test := range []struct {
		name   string
		wanted int
		input  string
	}{
		{
			"sample",
			21,
			`30373
25512
65332
33549
35390`,
		},
		{
			"find everything but the 1",
			14,
			`99999
25512
99999`,
		},
		{
			"find all, middle tallest",
			9,
			`111
121
111`,
		},
		{
			"find all but lowest middle",
			8,
			`111
101
111`,
		},
		{
			"find all but equal middle",
			8,
			`111
111
111`,
		},
	} {
		if got := solve(test.input); got != test.wanted {
			t.Run(test.name, func(t *testing.T) {
				t.Errorf("wanted %d, got %d", test.wanted, got)
			})
		}
	}

}
