package main

import (
	"fmt"
	"os"
	"strings"
)

const (
	off uint8 = iota
	on
)

func matrix(input []string) [][]uint8 {
	matr := make([][]uint8, len(input))
	for y := 0; y < len(input); y++ {
		matr[y] = make([]uint8, len(input[0]))
		for x := 0; x < len(input[0]); x++ {
			matr[y][x] = off
		}
	}
	return matr
}

func solve(inp string) int {
	input := strings.Split(strings.TrimSpace(inp), "\n")
	answer := matrix(input)
	fmt.Printf("input len(): %d, len([0]): %d\n", len(input), len(input[0]))

	// left to right
	for y := 0; y < len(input); y++ {
		var maxHeight byte
		// light up the edges
		answer[y][0] = on
		answer[y][len(input[0])-1] = on

		for x := 0; x < len(input[0]); x++ {
			currHeight := input[y][x]
			if x < len(input[0])-1 && currHeight > maxHeight {
				if y == 1 && x == 11 {
					fmt.Println("ltr bug")
				}
				maxHeight = currHeight
				answer[y][x] = on
			}
		}
	}

	// right to left
	for y := 0; y < len(input); y++ {
		var maxHeight byte
		for x := len(input[0]) - 1; x >= 0; x-- {
			currHeight := input[y][x]
			if x > 0 && currHeight > maxHeight {
				fmt.Printf("rtl bug y=%d; x=%d, fixed x=%d, x len=%d, maxHeight=%c, curr=%c\n", y, x, (len(input[0])-1)-x, len(input[0]), maxHeight, currHeight)
				maxHeight = currHeight
				answer[y][(len(input[0])-1)-x] = on
			}
		}
	}

	// top to bottom
	for x := 0; x < len(input[0]); x++ {
		var maxHeight byte
		// light up the edges
		answer[0][x] = on
		answer[len(input)-1][x] = on
		for y := 0; y < len(input); y++ {
			currHeight := input[y][x]
			if y < len(input)-1 && currHeight > maxHeight {
				if y == 1 && x == 11 {
					fmt.Println("top bug")
				}
				maxHeight = currHeight
				answer[y][x] = on
			}
		}
	}

	// bottom to top
	for x := 0; x < len(input[0]); x++ {
		var maxHeight byte
		for y := len(input) - 1; y >= 0; y-- {
			currHeight := input[y][x]
			if y > 0 && currHeight > maxHeight {
				if (len(input)-1)-y == 1 && x == 11 {
					fmt.Println("bottom bug")
				}
				maxHeight = currHeight
				answer[(len(input)-1)-y][x] = on
			}
		}
	}

	colorReset := "\033[0m"
	colorRed := "\033[31m"
	_ = "\033[32m"

	ons := 0
	for y := 0; y < len(input); y++ {
		for x := 0; x < len(input[0]); x++ {
			color := colorReset
			if answer[y][x] == on {
				color = colorRed
				ons++
			}
			if y == 1 && x == 11 {
				//color = colorX
			}
			fmt.Printf("%s%c%s", color, input[y][x], colorReset)
		}
		fmt.Printf("\n")
	}

	fmt.Println(ons)
	return ons
}

func main() {
	inp, err := os.ReadFile("input.txt")
	if err != nil {
		panic(err)
	}
	//TODO 1615 TO HIGH
	solve(string(inp))
}
