package main

import (
	"fmt"
)

var samples = map[string]int{
	"mjqjpqmgbljsphdztnvjfqwrcgsmlb":    7,
	"nppdvjthqldpwncqszvftbrmjlhg":      6,
	"nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg": 10,
	"zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw":  11,
}

func solve(in string) int {
	chars := 14
	seen := make(map[byte]int)
	for p := 0; p < len(in); {
		for c := 0; c < chars; c++ {
			dupePosition, isDupe := seen[in[p+c]]
			if isDupe {
				p = dupePosition + 1
				seen = make(map[byte]int)
				break
			} else if c == chars-1 {
				// result is 1 indexed, p is not
				return p + c + 1
			}
			seen[in[p+c]] = p + c
		}
	}
	panic(fmt.Sprintf("did not find a sequence in %s\n", in))
}

func main() {
	for in, out := range samples {
		if result := solve(in); result != out {
			panic(fmt.Sprintf("wanted %d, got %d\n", out, result))
		}
	}
}
