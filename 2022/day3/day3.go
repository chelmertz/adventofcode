package main

import (
	"strings"

	"gitlab.com/chelmertz/adventofcode/2022/aoc"
)

const (
	a           = int('a')
	z           = int('z')
	lowerOffset = a - 1
	A           = int('A')
	Z           = int('Z')
	upperOffset = A - 1
)

func prio(r rune) int {
	ascii := int(r)
	aoc.AssertTrue((ascii >= a && ascii <= z) || (ascii >= A && ascii <= Z))
	var point int
	if ascii >= a && ascii <= z {
		point = ascii - lowerOffset
	} else {
		point = ascii - upperOffset + 26
	}

	//fmt.Printf("point for %c is %d\n", r, point)
	aoc.AssertTrue(point > 0)
	return point
}

func answerA(text string) int {
	score := 0

	for _, line := range strings.Split(text, "\n") {
		c1 := line[0 : len(line)/2]
		c2 := line[len(line)/2:]
		aoc.AssertTrue(len(c1) == len(c2))
		s1 := aoc.NewRuneSet(c1)
		s2 := aoc.NewRuneSet(c2)
		existsInBoth := s1.Intersect(s2)
		aoc.AssertTrue(existsInBoth.Len() == 1)
		existsInBoth.Each(func(e rune) bool {
			score += prio(e)
			return false
		})
	}
	aoc.AssertTrue(score > 0)
	return score
}

func answerB(text string) int {
	score := 0
	threesomes := aoc.NewSet[rune]()

	for i, line := range strings.Split(text, "\n") {
		toMod := i + 1
		thisLineSet := aoc.NewRuneSet(line)
		if toMod%3 == 0 {
			threesomes = threesomes.Intersect(thisLineSet)
			aoc.AssertTrue(threesomes.Len() == 1)
			threesomes.Each(func(e rune) bool {
				score += prio(e)
				return false
			})
			threesomes = aoc.NewSet[rune]()
		} else if i%3 == 0 {
			threesomes = thisLineSet
		} else {
			threesomes = threesomes.Intersect(thisLineSet)
		}
	}
	aoc.AssertTrue(score > 0)
	return score
}
