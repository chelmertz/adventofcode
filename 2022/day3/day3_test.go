package main

import (
	"fmt"
	"testing"

	"gitlab.com/chelmertz/adventofcode/2022/aoc"
)

func Test_a_fixture(t *testing.T) {
	test := `vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw`

	wanted := 157
	if actual := answerA(test); wanted != actual {
		t.Errorf("got %d, wanted %d", actual, wanted)
	}
}

func Test_a_input(t *testing.T) {
	real := aoc.InputTxt()
	wanted := 8185
	if actual := answerA(real); wanted != actual {
		t.Errorf("got %d, wanted %d", actual, wanted)
	}
}

func Test_b_fixture(t *testing.T) {
	test1 := `vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg`
	test2 := `wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw`

	wanted1 := 18
	if actual1 := answerB(test1); wanted1 != actual1 {
		t.Errorf("got %d, wanted %d", actual1, wanted1)
	}

	wanted2 := 52
	if actual2 := answerB(test2); wanted2 != actual2 {
		t.Errorf("got %d, wanted %d", actual2, wanted2)
	}
}

func Test_b_input(t *testing.T) {
	real := aoc.InputTxt()
	fmt.Println(answerB(real))
}
