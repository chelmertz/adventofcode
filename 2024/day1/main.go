package main

import "strings"
import "fmt"
import "slices"
import "os"

func first(input string) {
	a, b := make([]int, 0), make([]int, 0)
	for _, line := range strings.Split(input, "\n") {
		var aa, bb int
		fmt.Sscanf(line, "%d %d", &aa, &bb)
		a = append(a, aa)
		b = append(b, bb)
	}
	slices.Sort(a)
	slices.Sort(b)
	diff := 0
	for i := range a {
		if a[i] > b[i] {
			diff += a[i]-b[i]
		} else {
			diff += b[i]-a[i]
		}
	}
	fmt.Println(diff)
}

func second(input string) {
	right := make(map[int]int)
	left := make([]int, 0)
	for _, line := range strings.Split(input, "\n") {
		var aa, bb int
		fmt.Sscanf(line, "%d %d", &aa, &bb)
		left = append(left, aa)
		right[bb] += 1
	}

	score := 0
	for _, num := range left {
		if count, ok := right[num]; ok {
			score += num*count
		}
	}

	fmt.Println(score)
}

func main() {
	input := `3   4
4   3
2   5
1   3
3   9
3   3`

	first(input)

	puzzle, _ := os.ReadFile("input.txt")
	first(string(puzzle))

	fmt.Println("second")
	second(input)
	second(string(puzzle))

}
