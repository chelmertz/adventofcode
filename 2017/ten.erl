-module(ten).
-export([start/0]).

start() -> 
	Numbers = lists:foldl(
	  fun (X, Acc) -> maps:put(X, X, Acc) end,
	  maps:new(),
	  lists:seq(0, 255)
	 ),
	ok = test(),
	Lengths = todo, % input
	iterate(Numbers, Lengths).

iterate(Numbers, Lengths) when is_list(Numbers) ->
	AsMap = lists:foldl(
	  fun (X, Acc) -> maps:put(maps:size(Acc), X, Acc) end,
	  maps:new(),
	  Numbers
	 ),
	iterate(AsMap, Lengths);
iterate(Numbers, Lengths) ->
	io:format("iterate/2: ~p ~p ~n", [Numbers, Lengths]),
	iterate(Numbers, 0, 0, Lengths).

iterate(Numbers, _CurrentPos, _SkipSize, []) ->
	maps:get(1, Numbers) * maps:get(2, Numbers);
iterate(Numbers, CurrentPos, SkipSize, [0|T]) ->
	iterate(Numbers, CurrentPos, SkipSize + 1, T);
iterate(Numbers, CurrentPos, SkipSize, Lengths) ->
	[Offset|Tl] = Lengths,
	{Switched, _LeftOver} = lists:foldl(
	  fun(CurrentPos_, Acc) ->
		  {Old, Offset_} = Acc,
		  io:format("~n~n new iteration~n"),
		  io:format("old: ~p ~n", [Old]),
		  io:format("offset: ~p ~n", [Offset_]),
		  io:format("current pos: ~p ~n", [CurrentPos_]),
		  SwitchWith = Offset_ rem 256,
		  io:format("switch with (index): ~p ~n", [SwitchWith]),
		  CurrentValue = maps:get(CurrentPos_, Old),
		  io:format("current value: ~p ~n", [CurrentValue]),
		  OffsetValue = maps:get(SwitchWith, Old),
		  io:format("offset value: ~p ~n", [OffsetValue]),
		  Updated = maps:put(CurrentPos_, OffsetValue, Old),
		  io:format("updated: ~p ~n", [Updated]),
		  Updated2 = maps:put(SwitchWith, CurrentValue, Updated),
		  io:format("updated2: ~p ~n", [Updated2]),
		  {Updated2, Offset_ - 1}
	  end,
	  {Numbers, Offset},
	  lists:seq(CurrentPos, (CurrentPos + Offset) div 2)
	 ),
	iterate(Switched, CurrentPos rem 256, SkipSize + 1, Tl).

test() ->
	12 = iterate([0, 1, 2, 3, 4], [3, 4, 1, 5]),
	ok.

