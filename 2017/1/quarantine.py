# retro:
# having 'first' in the first solution was so silly, ofc it would not be enough for the second solution
#
# the common denominator was the comparator for matching the current value

def peek_next(env, current):
    if env['prev'] == current:
        env['total'] += current
    if env['length'] == env['curr']:
        if current == env['first']:
            env['total'] += current

    env['prev'] = current
    env['curr'] += 1
    return env

def captcha(number_string):
    assert len(number_string) > 0

    d = map(int, list(number_string))

    print("reducing", number_string)
    res = reduce(peek_next, d, {'prev': None, 'first': d[0], 'length': len(number_string), 'curr': 1, 'total': 0})['total']
    print("result", res)
    return res

def part_two(number_string):
    assert len(number_string) > 0
    assert len(number_string) % 2 == 0

    d = map(int, list(number_string))

    total = 0
    length = len(d)
    for i in xrange(length):
        if d[i] == d[compare_with(i, length)]:
            total += d[i]
    return total

def compare_with(current, length):
    # for part one: offset = length + 1
    offset = length / 2
    return (current + offset) % length

### hints from aoc
assert 3 == captcha('1122')
assert 4 == captcha('1111')
assert 0 == captcha('1234')
assert 9 == captcha('91212129')
assert 1158 == captcha(open('input.txt').read().strip())

### part two helper
assert 2 == compare_with(0, 4)
assert 1 == compare_with(0, 2)
assert 0 == compare_with(2, 4)
assert 1 == compare_with(3, 4)

### part two
assert 6 == part_two('1212')
assert 0 == part_two('1221')
assert 4 == part_two('123425')
assert 12 == part_two('123123')
assert 4 == part_two('12131415')
assert 1132 == part_two(open('input.txt').read().strip())

##################################### ideal

import functools

def ideal(number_string, offset_comparator):
    assert len(number_string) > 0
    assert len(number_string) % 2 == 0

    d = map(int, list(number_string))

    total = 0
    length = len(d)
    compare_with = functools.partial(offset_comparator, length)
    for i in xrange(length):
        if d[i] == d[compare_with(i)]:
            total += d[i]
    return total

def p1_cmp(length, current):
    offset = 1
    return (current + offset) % length

def p2_cmp(length, current):
    offset = length / 2
    return (current + offset) % length

assert 3 == ideal('1122', p1_cmp)
assert 4 == ideal('1111', p1_cmp)
assert 0 == ideal('1234', p1_cmp)
assert 9 == ideal('91212129', p1_cmp)
assert 1158 == ideal(open('input.txt').read().strip(), p1_cmp)

### part two
assert 6 == ideal('1212', p2_cmp)
assert 0 == ideal('1221', p2_cmp)
assert 4 == ideal('123425', p2_cmp)
assert 12 == ideal('123123', p2_cmp)
assert 4 == ideal('12131415', p2_cmp)
assert 1132 == ideal(open('input.txt').read().strip(), p2_cmp)
