def count_points(stream):
    assert len(stream) > 0

    points = 0
    level = 0
    ignore_next = False
    in_comment = False
    commented_out = 0

    for char in stream:
        if ignore_next:
            ignore_next = False
            continue

        if char == '!':
            ignore_next = True
            continue

        if char == '<' and not in_comment:
            in_comment = True
            continue

        if char == '>' and in_comment:
            in_comment = False
            continue

        if in_comment:
            commented_out += 1
            continue

        if char == '{':
            level += 1
            continue

        if char == '}' and level > 0:
            points += level
            level -= 1
            continue

    
    assert points >= 0
    assert commented_out >= 0

    return points, commented_out

assert 1 == count_points('{}')[0]
assert 6 == count_points('{{{}}}')[0]
assert 5 == count_points('{{},{}}')[0]
assert 16 == count_points('{{{},{},{{}}}}')[0]
assert 1 == count_points('{<a>,<a>,<a>,<a>}')[0]
assert 9 == count_points('{{<ab>},{<ab>},{<ab>},{<ab>}}')[0]
assert 9 == count_points('{{<!!>},{<!!>},{<!!>},{<!!>}}')[0]
assert 3 == count_points('{{<a!>},{<a!>},{<a!>},{<ab>}}')[0]

assert (7640, 4368) == count_points(open('input.txt').read())
