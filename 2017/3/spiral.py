from collections import namedtuple

# retro:
# - if we're taking lots of stuff and output a single value:
#   - code a single iteration as a function
#   - add design by contract-like assertions
#   - use immutable data structures, or at least do not work on the input
#   - follow up to previous bullet: do no use state!!1 :)
#   - assert the single iteration function for a couple of manual steps
#   - switch between using "python spiral.py" and "pytest spiral.py"
#   - reduce the starting value by the single iteration function "until predicate"

def distance(position): return abs(position[0]) + abs(position[1])

def east(pos): return (pos[0]+1, pos[1])
def north(pos): return (pos[0], pos[1] + 1)
def west(pos): return (pos[0]-1, pos[1])
def south(pos): return (pos[0], pos[1] - 1)

def turn_ccw(direction):
    return {
            east: north,
            north: west,
            west: south,
            south: east}[direction]

current = namedtuple('current', [
    'number',        # current number: 1
    'direction',     # current direction, where do we find the higher number?: east
    'position',      # current position: (0, 0)
    'line_length',   # current line length: 2 (and then 2, 3, 3, 4, 4, ...)
    'line_position', # current position on line (1 based, so we can match "current pos == line length")
    'next'           # next line length, including the starting position, i.e. both last in current line, and first in next
    ])


def step(curr):
    if curr.line_position == curr.line_length:
        # turn and step!
        new_direction=turn_ccw(curr.direction)
        stepped = current(
                number=curr.number + 1,
                direction=new_direction,
                position=new_direction(curr.position),
                line_length=curr.next,
                line_position=2,
                next=curr.next + 1 if curr.next == curr.line_length else curr.next
                )
    else:
        # continue on line!
        stepped = current(
                number=curr.number + 1,
                direction=curr.direction,
                position=curr.direction(curr.position),
                line_length=curr.line_length,
                line_position=curr.line_position + 1,
                next=curr.next
                )

    assert stepped.number > curr.number
    assert stepped.position != curr.position
    assert stepped.next >= curr.line_length

    return stepped

starting_pos = current(number=1, direction=east, position=(0, 0), line_length=2, line_position=1, next=2)

# print(starting_pos)
# print(step(starting_pos))
# print(step(step(starting_pos)))
# print(step(step(step(starting_pos))))
# print(step(step(step(step(starting_pos)))))
# print(step(step(step(step(step(starting_pos))))))
# that looks nice.. now, let's try iterating

def position_of_number(curr, wanted):
    while wanted > curr.number:
        curr = step(curr)
    return curr.position

    # lol u dumb.. python doesn't have tail recursion!
    #return position_of_number(step(curr), wanted)

# for x in range(1, 25):
#     print("position of", x, position_of_number(starting_pos, x))


assert 0 == distance(position_of_number(starting_pos, 1))
assert 3 == distance(position_of_number(starting_pos, 12))
assert 2 == distance(position_of_number(starting_pos, 23))
assert 31 == distance(position_of_number(starting_pos, 1024))
assert 438 == distance(position_of_number(starting_pos, 265149))

# test helpers
assert (1, 0) == east((0, 0))
assert (0, 1) == north((0, 0))
assert (-1, 0) == west((0, 0))
assert (0, -1) == south((0, 0))


# part two

# 17  16  15  14  13
# 18   5   4   3  12
# 19   6   1   2  11
# 20   7   8   9  10
# 21  22  23  24  25



# idea:
# - make a first pass with a list of *a bunch* of calls to the first step(), store results in a list (at least the position tuple)
# - figure out the relationship between line_length, line_position and the positions closer to the center
# - traverse the list of positions in the correct order (same as from step())
# - add a new list with only numbers, numbers and positions; modify the old list's numbers; something else



def calc_number(curr):
    pass

assert 1 == step_through(starting_pos, 1)
assert 1 == step_through(starting_pos, 2)
assert 2 == step_through(starting_pos, 3)
assert 4 == step_through(starting_pos, 5)
