import collections
import operator

actions = {'inc': operator.add, 'dec': operator.sub}
comparators = {
    '<': operator.lt,
    '<=': operator.le,
    '>': operator.gt,
    '>=': operator.ge,
    '!=': operator.ne,
    '==': operator.eq,
    }

def run(instructions):
    # let's execute all instructions right after parsing them
    registers = collections.defaultdict(int)
    highest_temporary_max = 0
    for line in instructions:
        # print(line, "and split", line.split(), "split len", len(line.split()))
        register, instruction, value, _if, register_to_cmp, cmp, cmp_value = line.split()
        if comparators[cmp](int(registers[register_to_cmp]), int(cmp_value)):
            registers[register] = actions[instruction](registers[register], int(value))
        current_max = max(registers.values())
        if current_max > highest_temporary_max:
            highest_temporary_max = current_max
    return registers, highest_temporary_max

part1_testinput = """b inc 5 if a > 1
a inc 1 if b < 5
c dec -10 if a >= 1
c inc -20 if c == 10"""

assert 1 == max(run(part1_testinput.splitlines())[0].values())
assert 4416 == max(run(open('input.txt').readlines())[0].values())

assert 5199 == run(open('input.txt').readlines())[1]
