def redistribute(buckets):
    bucket_count = len(buckets)
    hashed = tuple(buckets)
    states_seen = set()
    iterations = 0
    while hashed not in states_seen:
        print("buckets!", buckets)
        states_seen.add(hashed)

        # find max
        bucket_with_highest_value = buckets.index(max(buckets))

        # balance from there, modulo the end position
        to_give_out = buckets[bucket_with_highest_value]
        next_index = (bucket_with_highest_value + 1) % bucket_count
        buckets[bucket_with_highest_value] = 0
        while to_give_out > 0:
            # todo: this is silly, traverse each bucket at most once instead..
            to_give_out -= 1
            buckets[next_index] += 1
            next_index = (next_index + 1) % bucket_count

        hashed = tuple(buckets)
        iterations += 1
    return (iterations, hashed)

assert 5 == redistribute(buckets=[0, 2, 7, 0])[0]


in_data = map(int, open('input.txt').read().split())

assert 3156 == redistribute(buckets=in_data)[0]

# part two
assert 1610 == redistribute(list(redistribute(buckets=in_data)[1]))[0]
