

def valid_passphrase(phrase):
    splitup = phrase.split()
    return len(splitup) == len(set(splitup))

def valid_passphrase2(phrase):
    splitup = phrase.split()
    anagrams = set()
    for w in splitup:
        anagrams.add(''.join(sorted(w)))
    return len(anagrams) == len(splitup)

inp = open('input.txt').readlines()
print len(filter(valid_passphrase, inp))

print len(filter(valid_passphrase2, inp))

assert False == valid_passphrase2("hej jeh ejh bla")
assert False == valid_passphrase2("hej jeh")
assert False == valid_passphrase2("hej jeh ejh")
assert True == valid_passphrase2("bla blu")
