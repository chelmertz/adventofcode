import itertools
import re

def parse(line):
    # i apparently suck at regexes, not sure how to match asdf, asdf2, asdf3 with separate regexes :/
    return re.match('^(\w+) \((\d+)\)(?: -> (.+))?', line).groups()

test_input1 = """pbga (66)
xhth (57)
ebii (61)
havc (66)
ktlj (57)
fwft (72) -> ktlj, cntj, xhth
qoyq (66)
padx (45) -> pbga, havc, qoyq
tknk (41) -> ugml, padx, fwft
jptl (61)
ugml (68) -> gyxo, ebii, jptl
gyxo (61)
cntj (57)"""

def stupid_p1_solution(matches):
    all_programs = set(map(lambda x: x[0], matches))
    all_programs_not_on_top = set(itertools.chain(*map(lambda x: x[2].split(', '), filter(lambda x: x[2] is not None, matches))))
    top = all_programs - all_programs_not_on_top
    assert 1 == len(top)
    return top.pop()

print(stupid_p1_solution(map(parse, test_input1.splitlines())))
print(stupid_p1_solution(map(parse, open('input.txt').readlines())))
