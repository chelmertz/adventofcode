
total = 0

def find_divisor(numbers):
    for i, n in enumerate(numbers):
        for first_in_tail in numbers[i+1:]:
            if n % first_in_tail == 0:
                return n/first_in_tail

for line in open('input.txt').readlines():
    numbers = reversed(sorted(map(int, line.strip().split())))
    total += find_divisor(list(numbers))

assert total == 3
