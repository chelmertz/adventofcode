import Control.Exception.Base (assert)
import Control.Monad (liftM, sequence)
import Data.List (find, sort)

-- retro:
-- - list items are separated with , ... that wasted some time
-- - stepping through the plan with one function at a time works great in haskell (for these small problems)
-- - good pattern:
--   - sketch algo on paper
--   - assert the result of the complete solution
--   - if the problem is too hard: break it up (and feel free to detect bad patterns, n^2 etc)
--   - create small functions for each step
--   - assert the results for each function (tdd)
--   - change the assertion of the complete solution to use the smaller functions
--   - post submission:
--     - what was especially hard? what worked? refactor the solutions of parts A and B, and learn what the common denominator was
-- - detecting pattern matching for lists should come more naturally, especially when you have done this stuff in erlang..
--
-- haskell stuff:
-- all@(x:xs) = [1, 2, 3]
--   all - [1, 2, 3]
--   x - 1
--   xs - [2, 3]
--
-- if something is "tainted" with Maybe (or any other monad?), it bubbles upwards, you can never get Int from Maybe Int etc

checksum :: String -> Int
checksum numbers =
  sum (map maxDiff (map lineToList (lines numbers)))

checksumFile :: String -> IO Int
checksumFile fileName =
  liftM checksum (readFile fileName)
  
lineToList :: String -> [Int]
lineToList line =
  map read $ words line

-- this is the relevant function to switch out for part 2
maxDiff :: [Int] -> Int
maxDiff numbers = maximum numbers - minimum numbers

-- part one
test =
  assert (["5 1 9 5", "7 5 3", "2 4 6 8"] == lines "5 1 9 5\n7 5 3\n2 4 6 8\n")
  assert ([5, 1, 9, 5] == lineToList "5 1 9 5")
  assert ([[5, 1, 9, 5], [7, 5, 3], [2, 4, 6, 8]] == map lineToList (lines "5 1 9 5\n7 5 3\n2 4 6 8\n"))
  assert (8 == maxDiff [5, 1, 9, 5])
  assert ([8, 4, 6] == map maxDiff (map lineToList (lines "5 1 9 5\n7 5 3\n2 4 6 8\n")))
  assert (18 == sum (map maxDiff (map lineToList (lines "5 1 9 5\n7 5 3\n2 4 6 8\n"))))
  assert (18 == checksum "5 1 9 5\n7 5 3\n2 4 6 8\n")
  True

-- part two
test2 =
  assert (Just 2 == evenFraction [10, 5])
  assert (evenFraction [10, 5] == evenFraction [5, 10])
  assert (Just 2 == evenFraction [6, 4, 7, 5, 10])
  assert (Just 3 == sumMaybeInt [(Just 3)])
  assert (Just 3 == evenFraction (lineToList "6 2 9 5\n"))
  --- hmm.. something is not right
  assert (Just 9 == evenFraction (map lineToList (lines "5 9 2 8\n9 4 7 3\n3 8 6 5")))
  True

-- trying to do something logarithmic (cmp to tail) instead of quadratic (permutations)
evenFraction :: [Int] -> Maybe Int
evenFraction numbers =
  let
    all@(largest:xs) = (reverse $ sort numbers)
    result = find (\smaller -> largest `mod` smaller == 0) xs
  in
    case result of
      Just a -> Just (largest `div` a)
      Nothing ->
        if length xs > 0
           then evenFraction xs
           else Nothing


-- from https://stackoverflow.com/questions/20034756/idiomatic-way-to-sum-a-list-of-maybe-int-in-haskell
sumMaybeInt :: [Maybe Int] -> Maybe Int
sumMaybeInt ints =
  fmap sum $ sequence ints

checksumPartTwo :: String -> Maybe Int
checksumPartTwo numbers =
  sumMaybeInt (map evenFraction (map lineToList (lines numbers)))
