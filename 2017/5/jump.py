def parse(jump_string):
    instructions = map(int, jump_string.splitlines())
    print instructions
    return instructions

def jump(position, instructions, steps_counter):
    if position < 0 or position >= len(instructions):
        return steps_counter

    next_position = instructions[position]
    instructions[position] += 1
    return jump(position + next_position, instructions, steps_counter + 1)

def iterative_jump(position, instructions, incrementer):
    steps_counter = 0
    number_of_instructions = len(instructions)
    while position >= 0 and position < number_of_instructions:
        next_position = instructions[position]
        instructions[position] = incrementer(next_position)
        position = position + next_position
        steps_counter += 1

    return steps_counter

def steps_for(jump_string, incrementer):
    # BOOOOO python is not tail recursive, this fails with
    # RuntimeError: maximum recursion depth exceeded
    # return jump(0, parse(jump_string), 0)

    return iterative_jump(0, parse(jump_string), incrementer)

assert [0, 3, 0, 1, -3] == parse("0\n3\n0\n1\n-3")

def increment_current_offset(value):
    return value + 1

assert 5 == steps_for("0\n3\n0\n1\n-3", increment_current_offset)
assert 376976 == steps_for(open("input.txt").read(), increment_current_offset)

## part two

def part_two_incrementer(value):
    if value >= 3:
        return value - 1
    else:
        return value + 1

assert 29227751 == steps_for(open("input.txt").read(), part_two_incrementer)
