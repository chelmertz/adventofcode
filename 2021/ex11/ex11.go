package main

import (
	"bufio"
	"container/list"
	"fmt"
	"io"
	"os"
)

type Grid struct {
	width, height int
	// [y][x]
	cells map[int]map[int]int
}

type Position struct {
	x, y int
}

func (p Position) Hash() int {
	return p.y*10 + p.x
}

func (g *Grid) neighboursOnGrid(origin Position) []Position {
	// TODO pass this array instead, dont allocate it over and over
	positions := make([]Position, 0)
	deltas := [][]int{
		{-1, -1},
		{-1, 0},
		{-1, 1},

		{1, -1},
		{1, 0},
		{1, 1},

		{0, -1},
		{0, 1},
	}

	for _, d := range deltas {
		newX, newY := origin.x+d[0], origin.y+d[1]
		if newX >= 0 && newX < g.width && newY >= 0 && newY < g.height {
			positions = append(positions, Position{x: newX, y: newY})
		}
	}

	return positions
}

func (g *Grid) Print() {
	for y := 0; y < g.height; y++ {
		for x := 0; x < g.width; x++ {
			fmt.Print(g.cells[y][x])
		}
		fmt.Println()
	}
	fmt.Println()
}

func (g *Grid) Flash(pos Position, toFlash *list.List) {
	neighbs := g.neighboursOnGrid(pos)
	for _, dir := range neighbs {
		g.cells[dir.y][dir.x]++
		if g.cells[dir.y][dir.x] > 9 {
			toFlash.PushBack(dir)
		}
	}
}

type CellSet map[int]bool

func (g *Grid) Tick() (flashedCells CellSet) {
	// keep this tick's flashes in a set
	flashedCells = make(CellSet)

	// TODO use a channel instead
	toFlash := list.New()
	toFlash.Init()

	// increase level
	for y := 0; y < g.height; y++ {
		for x := 0; x < g.width; x++ {
			g.cells[y][x]++

			if g.cells[y][x] > 9 {
				toFlash.PushBack(Position{x, y})
			}
		}
	}

	// flash neighbours
	for curr := toFlash.Front(); curr != nil; curr = curr.Next() {
		pos := curr.Value.(Position)
		if !flashedCells[pos.Hash()] {
			flashedCells[pos.Hash()] = true
			g.Flash(pos, toFlash)
		}
	}

	// decrease level
	for y := 0; y < g.height; y++ {
		for x := 0; x < g.width; x++ {
			if g.cells[y][x] > 9 {
				g.cells[y][x] = 0
			}
		}
	}

	return flashedCells
}

func NewGrid(reader io.Reader) *Grid {
	scanner := bufio.NewScanner(reader)

	grid := &Grid{
		width:  0,
		height: 0,
		cells:  make(map[int]map[int]int),
	}

	var y int
	for scanner.Scan() {
		grid.cells[y] = make(map[int]int)

		line := scanner.Text()
		if y == 0 {
			grid.width = len(line)
		}

		for x, number := range line {
			grid.cells[y][x] = int(number - '0')
		}

		y++
	}

	grid.height = y

	return grid
}

func Solve11A(reader io.Reader) (sumFlashes, allFlashesAtStep int) {
	grid := NewGrid(reader)

	flashed := 0
	for flashed != grid.height*grid.width {
		flashed = len(grid.Tick())
		allFlashesAtStep++
		if allFlashesAtStep <= 100 {
			sumFlashes += flashed
		}
	}

	return sumFlashes, allFlashesAtStep
}

func main() {
	a, b := Solve11A(os.Stdin)
	fmt.Println(a, b)
}
