package main

import (
	"strings"
	"testing"
)

func TestSolve11A(t *testing.T) {
	actual, _ := Solve11A(strings.NewReader(`5483143223
2745854711
5264556173
6141336146
6357385478
4167524645
2176841721
6882881134
4846848554
5283751526`))

	if wanted := 1656; wanted != actual {
		t.Errorf("Wanted %d, got %d", wanted, actual)
	}
}
