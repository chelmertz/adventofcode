package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"sort"
	"strings"
)

func solveLineEasy(line string) int {
	parts := strings.Split(line, " | ")
	numbersToDecode := strings.Split(parts[1], " ")
	digitsWithUniqueSegmentCount := 0

	//fmt.Printf("Whole line: %s\n", line)
	for _, chunk := range numbersToDecode {
		//fmt.Printf("chunk: %s\n", chunk)
		l := len(chunk)
		if l == 2 || l == 4 || l == 3 || l == 7 {
			digitsWithUniqueSegmentCount++
		}
	}
	return digitsWithUniqueSegmentCount
}

type SetChar map[rune]bool

var numbersAsStrings = map[int]string{
	0: "abcefg",
	1: "cf",
	2: "acdeg",
	3: "acdfg",
	4: "bcdf",
	5: "abdfg",
	6: "abdefg",
	7: "acf",
	8: "abcdefg",
	9: "abcdfg",
}

func SortString(toSort string) string {
	bytes := []byte(toSort)
	sort.Slice(bytes, func(i, j int) bool {
		return bytes[i] < bytes[j]
	})
	return string(bytes)
}

type Decoder struct {
	cipher map[string]int
}

func NewDecoder(a, b, c, d, e, f, g SetChar) *Decoder {
	decodingMap := map[rune]rune{
		'a': a.rune(),
		'b': b.rune(),
		'c': c.rune(),
		'd': d.rune(),
		'e': e.rune(),
		'f': f.rune(),
		'g': g.rune(),
	}
	cipher := make(map[string]int)
	// example for non-scrambled (the key must be sorted): result["cf"] = 1, result["abcefg"] = 0 etc
	for number, asString := range numbersAsStrings {
		var key strings.Builder
		for _, char := range asString {
			_, err := key.WriteRune(decodingMap[char])
			if err != nil {
				panic(fmt.Sprintf("Could not write rune %c to builder %v", char, key))
			}
		}
		cipher[SortString(key.String())] = number
	}

	return &Decoder{
		cipher,
	}
}

func (d *Decoder) Decode(scrambled string) int {
	charsSorted := SortString(scrambled)
	decoded, ok := d.cipher[charsSorted]
	if !ok {
		panic(fmt.Sprintf("Could not lookup %s in %v", charsSorted, d.cipher))
	}
	return decoded
}

func NewSetCharOfString(chars string) SetChar {
	set := make(SetChar)
	for _, char := range chars {
		set[char] = true
	}
	return set
}

func (s SetChar) rune() rune {
	if len(s) != 1 {
		panic(fmt.Sprintf("Cannot map setChar %v to a single rune, len %d != 1", s, len(s)))
	}
	var theRune rune
	for char := range s {
		theRune = char
	}
	return theRune
}

// Returns those only in "s", the complement of a
func (s SetChar) Subtract(a SetChar) SetChar {
	result := make(SetChar)
	for char := range s {
		if _, ok := a[char]; !ok {
			result[char] = true
		}
	}
	return result
}

func (s SetChar) Union(a SetChar) SetChar {
	result := make(SetChar)
	for char := range s {
		result[char] = true
	}
	for char := range a {
		result[char] = true
	}
	return result
}

func (s SetChar) Intersection(a SetChar) SetChar {
	result := make(SetChar)
	for char := range s {
		if _, ok := a[char]; ok {
			result[char] = true
		}
	}
	return result
}

func assert(actual, expected int) {
	if actual != expected {
		panic(fmt.Sprintf("wanted %v, got %v", actual, expected))
	}
}

func solveLineHard(line string) int {
	parts := strings.Split(line, " | ")
	input := strings.Split(parts[0], " ")

	all := SetChar{
		'a': true,
		'b': true,
		'c': true,
		'd': true,
		'e': true,
		'f': true,
		'g': true,
	}

	maybe0 := make(SetChar)
	maybe6 := make(SetChar)
	maybe9 := make(SetChar)

	var cf, acf, bcdf SetChar

	//fmt.Printf("Whole line: %s\n", line)
	for _, chunk := range input {
		//fmt.Printf("chunk: %s\n", chunk)
		switch l := len(chunk); l {
		case 2:
			cf = NewSetCharOfString(chunk)
		case 3:
			acf = NewSetCharOfString(chunk)
		case 4:
			bcdf = NewSetCharOfString(chunk)
		case 6:
			if len(maybe0) == 0 {
				for _, char := range chunk {
					maybe0[char] = true
				}
			} else if len(maybe6) == 0 {
				for _, char := range chunk {
					maybe6[char] = true
				}
			} else if len(maybe9) == 0 {
				for _, char := range chunk {
					maybe9[char] = true
				}
			} else {
				panic("already filled all 6-char possibilities")
			}
		}
	}

	var a, b, c, d, e, f, g SetChar

	a = acf.Subtract(cf)

	// intersection of 0, 6, 9 misses: d (from 0), c (from 6), e (from 9)
	// if we combine those missed out on (d, c, e) and intersect with
	// intersection of 1 and 7 (c, f), we get c (and also f, because it's the other one)
	missingDce := maybe0.Intersection(maybe6).Intersection(maybe9)
	dce := all.Subtract(missingDce)

	c = dce.Intersection(cf)
	de := dce.Subtract(c)
	f = cf.Subtract(c)

	// known at this point: a, c, f
	bd := bcdf.Subtract(cf)
	d = bd.Intersection(dce)
	b = bd.Subtract(d)
	e = de.Subtract(d)

	// known at this point: a, b, c, d, e, f; unknown: g
	//g := all - 4 - a - e
	g = all.Subtract(bcdf).Subtract(a).Subtract(e)

	// map where [a] => b means "a" in the instructions and "b" is the representation in the current line
	// only stores the completely decoded letters
	decoder := NewDecoder(a, b, c, d, e, f, g)
	//fmt.Printf("Got the decoder %v\n", decoder)

	sum := 0
	numbersToDecode := strings.Split(parts[1], " ")
	factor := 1000

	for _, chars := range numbersToDecode {
		subSum := decoder.Decode(chars) * factor
		//fmt.Printf("Got sub sum %d for chars %s\n", subSum, string(chars))
		sum += subSum
		factor /= 10
	}
	//fmt.Printf("Got whole sum: %d for numbers to decode: %v\n\n\n", sum, numbersToDecode)

	return sum
}

func Solve8A(reader io.Reader) (int, error) {
	scanner := bufio.NewScanner(reader)
	sum := 0
	for scanner.Scan() {
		line := scanner.Text()
		sum += solveLineEasy(line)
	}

	return sum, nil
}

func Solve8B(reader io.Reader) (int, error) {
	scanner := bufio.NewScanner(reader)
	sum := 0
	for scanner.Scan() {
		line := scanner.Text()
		sum += solveLineHard(line)
	}

	return sum, nil
}

func main() {
	res, err := Solve8B(os.Stdin)

	if err != nil {
		panic(err)
	}

	fmt.Println(res)
}
