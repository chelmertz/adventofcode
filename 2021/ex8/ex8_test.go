package main

import (
	"reflect"
	"strings"
	"testing"
)

func TestSolve8A(t *testing.T) {
	testInput := strings.NewReader(`be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce`)
	res, err := Solve8A(testInput)

	if err != nil {
		t.Errorf("Err not nil: %v", err)
	}

	if wanted := 26; res != wanted {
		t.Errorf("Wanted %d, got %d", wanted, res)
	}
}

func TestUnion_NoMatches(t *testing.T) {
	a := make(SetChar)

	b := make(SetChar)
	b['a'] = true

	expected := b
	actual := a.Union(b)

	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("Wanted %v, got %v", expected, actual)
	}
}

func TestUnion_OneMatch(t *testing.T) {
	a := make(SetChar)
	a['a'] = true
	a['b'] = true
	a['c'] = true

	b := make(SetChar)
	b['a'] = true

	expected := a
	actual := a.Union(b)

	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("Wanted %v, got %v", expected, actual)
	}
}

func TestIntersection_NoMatches(t *testing.T) {
	a := make(SetChar)

	b := make(SetChar)
	b['a'] = true

	expected := a
	actual := a.Intersection(b)

	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("Wanted %v, got %v", expected, actual)
	}
}

func TestIntersection_OneMatch(t *testing.T) {
	a := make(SetChar)
	a['a'] = true
	a['b'] = true
	a['c'] = true

	b := make(SetChar)
	b['a'] = true

	expected := b
	actual := a.Intersection(b)

	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("Wanted %v, got %v", expected, actual)
	}
}

func TestSubtract_NoMatches(t *testing.T) {
	a := make(SetChar)

	b := make(SetChar)
	b['a'] = true

	expected := a
	actual := a.Subtract(b)

	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("Wanted %v, got %v", expected, actual)
	}
}

func TestSubtract_OneMatch(t *testing.T) {
	a := make(SetChar)
	a['a'] = true
	a['b'] = true
	a['c'] = true

	b := make(SetChar)
	b['a'] = true

	expected := make(SetChar)
	expected['b'] = true
	expected['c'] = true

	actual := a.Subtract(b)

	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("Wanted %v, got %v", expected, actual)
	}
}

func TestSortString(t *testing.T) {
	wantedActual := map[string]string{
		"abc": SortString("bca"),
		"aa":  SortString("aa"),
		"":    SortString(""),
	}

	for wanted, actual := range wantedActual {
		if wanted != actual {
			t.Errorf("Wanted %s, got %s", wanted, actual)
		}
	}
}

func TestSolve8B_Example(t *testing.T) {
	testInput := strings.NewReader("acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf")
	res, err := Solve8B(testInput)

	if err != nil {
		t.Errorf("Err not nil: %v", err)
	}

	if wanted := 5353; res != wanted {
		t.Errorf("Wanted %d, got %d", wanted, res)
	}
}

func TestSolve8B(t *testing.T) {
	testInput := strings.NewReader(`be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce`)
	res, err := Solve8B(testInput)

	if err != nil {
		t.Errorf("Err not nil: %v", err)
	}

	if wanted := 61229; res != wanted {
		t.Errorf("Wanted %d, got %d", wanted, res)
	}
}
