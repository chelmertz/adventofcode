package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"sort"
)

var matching = map[rune]rune{
	')': '(',
	']': '[',
	'}': '{',
	'>': '<',
}

var matchingReversed = map[rune]rune{
	'(': ')',
	'[': ']',
	'{': '}',
	'<': '>',
}

var penaltyA = map[rune]int{
	')': 3,
	']': 57,
	'}': 1197,
	'>': 25137,
}

var penaltyB = map[rune]int{
	')': 1,
	']': 2,
	'}': 3,
	'>': 4,
}

func MatchLine(line string, abortOnError bool) (nextToken rune, stack []rune, consumedAll bool) {
	for _, char := range line {
		var isClosing rune

		switch char {
		case '}', ')', ']', '>':
			isClosing = char
		default:
			stack = append(stack, char)
		}

		if isClosing != 0 {
			if len(stack) > 0 {
				head := len(stack) - 1
				if stack[head] == matching[isClosing] {
					stack = stack[:head]
				} else {
					return isClosing, stack, false
				}
			} else {
				return isClosing, stack, false
			}
		}
	}
	// added consumedAll as a return value, to avoid relying on nextToken == 0 being a sentinel
	return 0, stack, true
}

func Solve10A(reader io.Reader) int {
	scanner := bufio.NewScanner(reader)

	sumOfPenalties := 0
	for scanner.Scan() {
		nextToken, _, ok := MatchLine(scanner.Text(), true)
		if !ok {
			sumOfPenalties += penaltyA[nextToken]
		}
	}
	return sumOfPenalties
}

func Solve10B(reader io.Reader) int {
	scanner := bufio.NewScanner(reader)

	var penalties []int
	for scanner.Scan() {
		_, stack, ok := MatchLine(scanner.Text(), false)
		if ok {
			sum := 0
			// sigh, go by the stack in reverse
			for i := len(stack) - 1; i >= 0; i-- {
				sum *= 5
				sum += penaltyB[matchingReversed[stack[i]]]
			}
			penalties = append(penalties, sum)
		}
	}
	sort.Ints(penalties)
	return penalties[len(penalties)/2]
}

func main() {
	fmt.Println(Solve10B(os.Stdin))
}
