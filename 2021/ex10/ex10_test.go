package main

import (
	"strings"
	"testing"
)

func TestSolve10A(t *testing.T) {
	actual := Solve10A(strings.NewReader(`[({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]`))

	if wanted := 26397; wanted != actual {
		t.Errorf("Wanted %d, got %d", wanted, actual)
	}
}

func TestExampleA1(t *testing.T) {
	actual := Solve10A(strings.NewReader("{([(<{}[<>[]}>{[]{[(<()>"))

	if wanted := 1197; wanted != actual {
		t.Errorf("Wanted %d, got %d", wanted, actual)
	}
}

func TestSolve10B(t *testing.T) {
	actual := Solve10B(strings.NewReader(`[({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]`))

	if wanted := 288957; wanted != actual {
		t.Errorf("Wanted %d, got %d", wanted, actual)
	}
}

func TestExampleB1(t *testing.T) {
	actual := Solve10B(strings.NewReader("<{([{{}}[<[[[<>{}]]]>[]]"))

	if wanted := 294; wanted != actual {
		t.Errorf("Wanted %d, got %d", wanted, actual)
	}
}
