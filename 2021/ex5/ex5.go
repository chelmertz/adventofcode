package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

func StringToIntOrPanic(number string) (numberAsInt int) {
	numberAsInt, err := strconv.Atoi(number)
	if err != nil {
		panic(err)
	}
	return
}

func parseLine(line string) (x1_y1_x2_y2 [4]int) {
	parts := strings.Fields(line)
	from := strings.Split(parts[0], ",")
	to := strings.Split(parts[2], ",")

	x1_y1_x2_y2[0] = StringToIntOrPanic(from[0])
	x1_y1_x2_y2[1] = StringToIntOrPanic(from[1])
	x1_y1_x2_y2[2] = StringToIntOrPanic(to[0])
	x1_y1_x2_y2[3] = StringToIntOrPanic(to[1])
	return
}

func measure(a, b int) (min, max, absDiff int) {
	if a > b {
		max = a
		min = b
	} else {
		max = b
		min = a
	}
	absDiff = max - min
	return
}

func KeyOf(x, y int) (key string) {
	key = fmt.Sprintf("%d-%d", x, y)
	return
}

func Solve5A(reader io.Reader) (int, error) {
	scanner := bufio.NewScanner(reader)

	allPoints := make(map[string]int)
	var maxX, maxY int

	for scanner.Scan() {
		line := scanner.Text()
		parts := parseLine(line)

		if parts[0] > maxX {
			maxX = parts[0]
		}
		if parts[2] > maxX {
			maxX = parts[2]
		}

		if parts[1] > maxY {
			maxY = parts[1]
		}
		if parts[3] > maxY {
			maxY = parts[3]
		}

		// instructions for 5A: only straight lines
		if parts[0] == parts[2] {
			min, max, _ := measure(parts[1], parts[3])
			for y := min; y <= max; y++ {
				allPoints[KeyOf(parts[0], y)] += 1
			}
		}

		// instructions for 5A: only straight lines
		if parts[1] == parts[3] {
			min, max, _ := measure(parts[0], parts[2])
			for x := min; x <= max; x++ {
				allPoints[KeyOf(x, parts[1])] += 1
			}
		}
	}

	fmt.Println(allPoints)

	var out strings.Builder
	for y := 0; y <= maxY; y++ {
		for x := 0; x <= maxX; x++ {
			val, found := allPoints[KeyOf(x, y)]
			if found {
				out.WriteString(strconv.Itoa(val))
			} else {
				out.WriteByte('.')
			}
		}
		out.WriteByte('\n')
	}

	//fmt.Print(out.String())
	fmt.Println(maxY, maxX)

	sum := 0
	for _, value := range allPoints {
		if value > 1 {
			sum++
		}
	}

	return sum, nil
}

func Solve5B(reader io.Reader) (int, error) {
	scanner := bufio.NewScanner(reader)

	allPoints := make(map[string]int)
	var maxX, maxY int

	for scanner.Scan() {
		line := scanner.Text()
		parts := parseLine(line)

		if parts[0] > maxX {
			maxX = parts[0]
		}
		if parts[2] > maxX {
			maxX = parts[2]
		}

		if parts[1] > maxY {
			maxY = parts[1]
		}
		if parts[3] > maxY {
			maxY = parts[3]
		}

		if parts[0] == parts[2] {
			min, max, _ := measure(parts[1], parts[3])
			for y := min; y <= max; y++ {
				allPoints[KeyOf(parts[0], y)] += 1
			}
		} else if parts[1] == parts[3] {
			min, max, _ := measure(parts[0], parts[2])
			for x := min; x <= max; x++ {
				allPoints[KeyOf(x, parts[1])] += 1
			}
		} else {
			// max av y & dess x -> y-1 & dess x mot x2 till de träffar
			min, max, _ := measure(parts[1], parts[3])
			xDelta := 1
			var x int

			if parts[1] == min {
				x = parts[0]
				if x > parts[2] {
					// we're going from y min to y max, and we need to no if we want to increment
					// or decrement x for each step
					xDelta = -1
				}
			} else {
				x = parts[2]
				if x > parts[0] {
					// we're going from y min to y max, and we need to no if we want to increment
					// or decrement x for each step
					xDelta = -1
				}
			}

			for y := min; y <= max; y++ {
				allPoints[KeyOf(x, y)] += 1
				x += xDelta
			}
		}
	}

	var out strings.Builder
	for y := 0; y <= maxY; y++ {
		for x := 0; x <= maxX; x++ {
			val, found := allPoints[KeyOf(x, y)]
			if found {
				out.WriteString(strconv.Itoa(val))
			} else {
				out.WriteByte('.')
			}
		}
		out.WriteByte('\n')
	}

	//fmt.Print(out.String())

	sum := 0
	for _, value := range allPoints {
		if value > 1 {
			sum++
		}
	}

	return sum, nil
}

func main() {
	res, err := Solve5B(os.Stdin)

	if err != nil {
		panic(err)
	}

	fmt.Println(res)
}
