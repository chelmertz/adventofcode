package main

import (
	"strings"
	"testing"
)

func TestA(t *testing.T) {
	res, err := Solve5A(strings.NewReader(`0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2`))

	if err != nil {
		t.Errorf("%v", err)
	}

	if wanted := 5; wanted != res {
		t.Errorf("Wanted %d, actual: %d", wanted, res)
	}
}

func TestB(t *testing.T) {
	res, err := Solve5B(strings.NewReader(`0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2`))

	if err != nil {
		t.Errorf("%v", err)
	}

	if wanted := 12; wanted != res {
		t.Errorf("Wanted %d, actual: %d", wanted, res)
	}
}
