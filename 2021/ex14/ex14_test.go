package main

import (
	"strings"
	"testing"
)

func TestA(t *testing.T) {
	a, _ := Solve14(strings.NewReader(`NNCB

CH -> B
HH -> N
CB -> H
NH -> C
HB -> C
HC -> B
HN -> C
NN -> C
BH -> H
NC -> B
NB -> B
BN -> B
BB -> N
BC -> B
CC -> N
CN -> C`))

	if wanted := 1588; wanted != a {
		t.Errorf("Wanted %d, got %d", wanted, a)
	}
}
