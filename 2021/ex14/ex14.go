package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

func Solve14(reader io.Reader) (a, b int) {
	scanner := bufio.NewScanner(reader)

	mappingCount := make(map[string]int)
	mapping := make(map[string]string)
	charCount := make(map[string]int)

	// first line
	scanner.Scan()
	template := scanner.Text()
	for i := 0; i < len(template)-1; i++ {
		mappingCount[template[i:i+2]]++
	}

	fmt.Println("mapping count at first", mappingCount, "for string", template)
	fmt.Println()

	// ignore empty line
	scanner.Scan()

	for scanner.Scan() {
		var key, value string
		fmt.Sscanf(scanner.Text(), "%s -> %s", &key, &value)
		mapping[key] = value
	}

	if err := scanner.Err(); err != nil {
		panic(err)
	}


	steps := 3
	for i := 0; i < steps; i++ {
		newCount := make(map[string]int)
		// this is buggy: it doesn't take everything into account
		// i.e. it gens 12 34 for every it, but should gen 12 23 34
		for key, count := range mappingCount {
			mapped := mapping[key]

			charCount[]

			// NN => C triggers NC++ and CN++
			right := string(key[0]) + mapped
			left := mapped + string(key[1])
			fmt.Println("mapped", key, "to", mapped, "giving us: ", right, "and", left)
			newCount[left] += count
			newCount[right] += count
		}
		mappingCount = newCount
		//for key, value := range newCount {
		//	mappingCount[key] += value
		//}
		fmt.Println("iteration", i, "mappingCount", mappingCount)
		//commons(CountsOf(mappingCount))
		fmt.Println()
	}

	mostCommon, leastCommon, _ := commons(charCount)

	fmt.Printf("mapping count\n%v\nfor template %s\n", mappingCount, template)

	a = charCount[mostCommon] - charCount[leastCommon]

	return
}

func commons(charCount map[string]int) (mostCommon, leastCommon string, total int) {
	for char, count := range charCount {
		total += count

		if mostCommon == "" || count > charCount[mostCommon] {
			mostCommon = char
		}

		if leastCommon == "" || count < charCount[leastCommon] {
			leastCommon = char
		}

		fmt.Println("count for", string(char), ":", count)
	}

	fmt.Println("total count", total)
	return
}

func CountsOf(counts map[string]int) map[rune]int {
	res := make(map[rune]int)
	for key, count := range counts {
		for _, c := range key {
			res[c] += count
		}
	}
	return res
}

func main() {
	a, b := Solve14(os.Stdin)
	fmt.Println(a, b)
}
