package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
)

func Solve1a(reader io.Reader) (int, error) {
	// read file line by line
	// translate all of line's contents to an integer
	// ^possible variation: numbers larger than ints
	// not probable, not costly, skip this for A
	// compare each line with the previous one
	// ^possible variation: compare with more/different lines
	// very probable for B, store things in memory instead of streaming

	scanner := bufio.NewScanner(reader)
	var previous, increments int
	hasPrev := false
	for scanner.Scan() {
		current, err := strconv.Atoi(scanner.Text())
		if err != nil {
			panic(err)
		}
		//fmt.Printf("previous: %v, current: %v\n", previous, current)
		if hasPrev {
			if current > previous {
				increments++
			}
		}
		hasPrev = true
		previous = current
	}

	if err := scanner.Err(); err != nil {
		return 0, err
	}

	return increments, nil
}

func putValue(value, index int, buf *[3]int) {
	buf[index%3] = value
}

func sumWithCurrent(buf *[3]int, index, current int) int {
	if index < 2 {
		return 0
	}
	sum := 0
	for index_, num := range buf {
		if index_ == index {
			sum += current
		} else {
			sum += num
		}
	}
	return sum
}

func Solve1b(reader io.Reader) (int, error) {
	scanner := bufio.NewScanner(reader)
	// "math is cheaper than allocations"

	var buffer [3]int
	var index, increments, previousSum int
	for scanner.Scan() {
		current, err := strconv.Atoi(scanner.Text())
		if err != nil {
			panic(err)
		}
		putValue(current, index, &buffer)
		currentSum := sumWithCurrent(&buffer, index, current)
		//fmt.Printf("previous sum: %v, current sum: %v, index: %d, current: %d, current buf %v\n", previousSum, currentSum, index, current, buffer)
		if currentSum > previousSum && previousSum != 0 {
			increments++
		}
		previousSum = currentSum
		index++
	}

	if err := scanner.Err(); err != nil {
		return 0, err
	}

	return increments, nil
}

func main() {
	result, err := Solve1b(os.Stdin)
	if err != nil {
		panic(err)
	}
	fmt.Println(result)
}
