package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

func commaNumbers(line string) []int {
	numbersAsString := strings.Split(line, ",")
	numbers := make([]int, 0)
	for _, value := range numbersAsString {
		number, err := strconv.Atoi(value)
		if err != nil {
			panic(err)
		}

		numbers = append(numbers, number)
	}
	return numbers
}

func Solve6A(reader io.Reader) (int, error) {
	scanner := bufio.NewScanner(reader)
	scanner.Scan()
	numbers := commaNumbers(scanner.Text())
	for day := 0; day < 80; day++ {
		// so expensive, probably too expensive, anyway: try to benchmark it
		// (lower the amount of days) just to see what it looks like
		// TODO what happens when we replace something in a loop
		for index, value := range numbers {
			if value == 0 {
				numbers[index] = 6
				numbers = append(numbers, 8)
			} else {
				numbers[index]--
			}
		}
	}

	return len(numbers), nil
}

func Solve6B(reader io.Reader) (int, error) {
	scanner := bufio.NewScanner(reader)
	scanner.Scan()
	numbers := commaNumbers(scanner.Text())

	// [0 => count0, 1 => count1, ...]
	// foreach day, swap counts, for the 0: also add to (replace rather, because
	// of the spec) 8 => count8
	fishPerDay := make(map[int]int)
	for _, value := range numbers {
		fishPerDay[value]++
	}
	fmt.Println(fishPerDay)

	for i := 0; i < 256; i++ {
		// just waste this allocation, rather than juggling indices/tmp vars
		newFishPerDay := make(map[int]int)
		for key, value := range fishPerDay {
			if key == 0 {
				newFishPerDay[8] = value
				newFishPerDay[6] += value
			} else {
				newFishPerDay[key-1] += value
			}
		}
		fishPerDay = newFishPerDay
	}

	sum := 0
	for _, value := range fishPerDay {
		sum += value
	}

	return sum, nil
}

func main() {
	res, err := Solve6B(os.Stdin)

	if err != nil {
		panic(err)
	}

	fmt.Println(res)
}
