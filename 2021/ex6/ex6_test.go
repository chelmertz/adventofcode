package main

import (
	"strings"
	"testing"
)

func TestA(t *testing.T) {
	res, err := Solve6A(strings.NewReader("3,4,3,1,2"))

	if err != nil {
		t.Errorf("%v", err)
	}

	if wanted := 5934; wanted != res {
		t.Errorf("Wanted %d, got %d", wanted, res)
	}
}

func TestB(t *testing.T) {
	res, err := Solve6B(strings.NewReader("3,4,3,1,2"))

	if err != nil {
		t.Errorf("%v", err)
	}

	if wanted := 26984457539; wanted != res {
		t.Errorf("Wanted %d, got %d", wanted, res)
	}
}
