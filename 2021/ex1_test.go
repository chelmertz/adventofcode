package main

import (
	"strings"
	"testing"
)

func TestExampleDataA(t *testing.T) {
	// not sure if it ends in newline
	input := strings.NewReader(`199
200
208
210
200
207
240
269
260
263`)
	result, err := Solve1a(input)
	if err != nil {
		t.Errorf("Got error: %v", err)
	}

	wanted := 7
	if result != wanted {
		t.Errorf("Wrong result, wanted %d, got: %v", wanted, result)
	}
}

func TestBufferB(t *testing.T) {
	testData := []int{1, 2, 3, 4, 5}
	// we are comparing a sliding window of *three* values
	expectedSums := []int{0, 0, 6, 9, 12}

	var buf [3]int
	for index, value := range testData {
		putValue(value, index, &buf)
		actual := sumWithCurrent(&buf, index, value)
		if actual != expectedSums[index] {
			t.Errorf("Wanted the sum %d, got %d, for the index %d in buffer %v", expectedSums[index], actual, index, buf)
		}
	}
}

func TestExampleDataB(t *testing.T) {
	input := strings.NewReader(`199
200
208
210
200
207
240
269
260
263`)
	result, err := Solve1b(input)
	if err != nil {
		t.Errorf("Got error: %v", err)
	}

	wanted := 5
	if result != wanted {
		t.Errorf("Wrong result, wanted %d, got: %v", wanted, result)
	}
}
