package main

import (
	"strings"
	"testing"
)

func Test2a(t *testing.T) {
	result, err := Solve2a(strings.NewReader(`forward 5
down 5
forward 8
up 3
down 8
forward 2`))

	if err != nil {
		t.Errorf("%s", err)
	}

	if result != 150 {
		t.Errorf("Error, wanted 150, got %d", result)
	}

}

func Test2b(t *testing.T) {
	result, err := Solve2b(strings.NewReader(`forward 5
down 5
forward 8
up 3
down 8
forward 2`))

	if err != nil {
		t.Errorf("%s", err)
	}

	if result != 900 {
		t.Errorf("Error, wanted 900, got %d", result)
	}

}
