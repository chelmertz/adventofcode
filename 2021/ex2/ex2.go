package main

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"strings"
)

// guessing 2B:
//  maybe another dimension
//  only some of the commands counts, or all commands count, but in a different order than 0..N
func Solve2a(reader io.Reader) (int, error) {
	scan := bufio.NewScanner(reader)

	var horizontal, depth int
	for scan.Scan() {
		line := scan.Text()
		parts := strings.Split(line, " ")
		if len(parts) != 2 {
			return 0, errors.New(fmt.Sprintf("Could not split '%s' into dir + steps", line))
		}

		direction := parts[0]
		steps, err := strconv.Atoi(parts[1])
		if err != nil {
			return 0, err
		}
		switch direction {
		case "forward":
			horizontal += steps
		case "down":
			depth += steps
		case "up":
			depth -= steps
		default:
			log.Fatalf("Weird direction: %s", direction)
		}
	}
	return horizontal * depth, nil
}

func Solve2b(reader io.Reader) (int, error) {
	scan := bufio.NewScanner(reader)

	var horizontal, depth, aim int
	for scan.Scan() {
		line := scan.Text()
		parts := strings.Split(line, " ")
		if len(parts) != 2 {
			return 0, errors.New(fmt.Sprintf("Could not split '%s' into dir + steps", line))
		}

		direction := parts[0]
		steps, err := strconv.Atoi(parts[1])
		if err != nil {
			return 0, err
		}
		switch direction {
		case "forward":
			horizontal += steps
			depth += aim * steps
		case "down":
			aim += steps
		case "up":
			aim -= steps
		default:
			log.Fatalf("Weird direction: %s", direction)
		}
	}
	return horizontal * depth, nil
}

func main() {
	result, err := Solve2b(os.Stdin)

	if err != nil {
		panic(err)
	}

	fmt.Println(result)
}
