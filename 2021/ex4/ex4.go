package main

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"strings"
)

type Xy struct {
	x, y int
}

type Board struct {
	// xy by number, this is removed when the number is called
	Unvisited map[int]*Xy
	// number by xy
	NumbersByXy map[string]int
}

func (xy *Xy) KeyHash() string {
	return fmt.Sprintf("%d-%d", xy.x, xy.y)
}

func NewBoard(fiveByFiveSpaceSeparatedNumbers []string) *Board {
	if len(fiveByFiveSpaceSeparatedNumbers) != 5 {
		log.Fatalf("NewBoard() wants five strings with five numbers each, got %v", fiveByFiveSpaceSeparatedNumbers)
	}

	board := &Board{
		make(map[int]*Xy),
		make(map[string]int),
	}

	for y := 0; y < 5; y++ {
		row := strings.Fields(fiveByFiveSpaceSeparatedNumbers[y])
		for x, numberAsString := range row {
			number, err := strconv.Atoi(numberAsString)
			if err != nil {
				log.Fatalf("Wanted to convert %s to a number, couldn't: %v", numberAsString, err)
			}
			xy := &Xy{x, y}

			board.Unvisited[number] = xy
			board.NumbersByXy[xy.KeyHash()] = number

			x++
		}

		if len(board.Unvisited) != (y+1)*5 {
			log.Fatalf("Expected Unvisited to be a multiple of 5 for iteration %d, got: %v", y, board.Unvisited)
		}
	}

	return board
}

func (b *Board) IsSolved() bool {
	for x := 0; x < 5; x++ {
		yIsEmpty := true
		for y := 0; y < 5; y++ {
			xy := fmt.Sprintf("%d-%d", x, y)
			if _, found := b.Unvisited[b.NumbersByXy[xy]]; found {
				yIsEmpty = false
			}
		}

		if yIsEmpty {
			return true
		}
	}

	for y := 0; y < 5; y++ {
		xIsEmpty := true
		for x := 0; x < 5; x++ {
			xy := fmt.Sprintf("%d-%d", x, y)
			if _, found := b.Unvisited[b.NumbersByXy[xy]]; found {
				xIsEmpty = false
			}
		}

		if xIsEmpty {
			return true
		}
	}

	return false
}

func (b *Board) SumOfNonMarked() int {
	sum := 0
	for number := range b.Unvisited {
		sum += number
	}
	return sum
}

func (b *Board) Mark(number int) (wasFound bool) {
	if _, wasFound := b.Unvisited[number]; wasFound {
		delete(b.Unvisited, number)
	}
	return wasFound
}

func NumbersToCall(commaSeparatedNumbers string) []int {
	numbersAsStrings := strings.Split(commaSeparatedNumbers, ",")
	numbers := make([]int, len(numbersAsStrings))
	for index, i := range numbersAsStrings {
		number, err := strconv.Atoi(i)
		if err != nil {
			panic(err)
		}
		numbers[index] = number
	}
	return numbers
}

func PlayBingo(numbers []int, boards []*Board) (int, error) {

	for i := 0; i < len(numbers); i++ {
		for boardId, board := range boards {
			// TODO make async
			board.Mark(numbers[i])
			if board.IsSolved() {
				sumLeft := board.SumOfNonMarked()
				fmt.Printf("board number %d won, sum left: %d\n", boardId, sumLeft)
				return sumLeft * numbers[i], nil
			}
		}
	}

	return 0, errors.New("No winner after all rounds")
}

func PlayBingoAndFindWorstBoard(numbers []int, boards []*Board) (int, error) {
	solvedBoards := make(map[int]struct{})
	for i := 0; i < len(numbers); i++ {
		for boardId, board := range boards {
			if _, solved := solvedBoards[boardId]; solved {
				continue
			}
			// TODO make async
			board.Mark(numbers[i])
			if board.IsSolved() {
				solvedBoards[boardId] = struct{}{}
				if len(solvedBoards) == len(boards) {
					sumLeft := board.SumOfNonMarked()
					fmt.Printf("board number %d got last, sum left: %d\n", boardId, sumLeft)
					return sumLeft * numbers[i], nil
				}
			}
		}
	}

	return 0, errors.New("No winner after all rounds")
}

func Solve4A(reader io.Reader) (result int, err error) {
	scanner := bufio.NewScanner(reader)
	var numbersToCall []int
	var boards []*Board
	var linesForBoard []string
	for scanner.Scan() {
		if numbersToCall == nil {
			numbersToCall = NumbersToCall(scanner.Text())
			continue
		}

		currentLine := scanner.Text()

		if currentLine == "" {
			continue
		}

		linesForBoard = append(linesForBoard, currentLine)

		if len(linesForBoard) == 5 {
			boards = append(boards, NewBoard(linesForBoard))
			linesForBoard = nil
		}
	}

	result, err = PlayBingo(numbersToCall, boards)
	return
}

func Solve4B(reader io.Reader) (result int, err error) {
	scanner := bufio.NewScanner(reader)
	var numbersToCall []int
	var boards []*Board
	var linesForBoard []string
	for scanner.Scan() {
		if numbersToCall == nil {
			numbersToCall = NumbersToCall(scanner.Text())
			continue
		}

		currentLine := scanner.Text()

		if currentLine == "" {
			continue
		}

		linesForBoard = append(linesForBoard, currentLine)

		if len(linesForBoard) == 5 {
			boards = append(boards, NewBoard(linesForBoard))
			linesForBoard = nil
		}
	}

	result, err = PlayBingoAndFindWorstBoard(numbersToCall, boards)
	return
}

func main() {
	result, err := Solve4B(os.Stdin)

	if err != nil {
		panic(err)
	}

	fmt.Println(result)
}
