package main

import (
	"strings"
	"testing"
)

func TestFolding(t *testing.T) {
	grid := NewGrid(strings.NewReader(`6,10
0,14
9,10
0,3
10,4
4,11
6,0
6,12
4,1
0,13
10,12
3,4
3,0
8,4
1,10
2,14
8,10
9,0`))

	//grid.Print()
	actual := len(grid.points)
	if wanted := 18; wanted != actual {
		t.Errorf("Wanted %d, got %d", wanted, actual)
	}

	grid = grid.fold(Fold{'y', 7})
	grid.Print()

	actual = len(grid.points)
	if wanted := 17; wanted != actual {
		t.Errorf("Wanted %d, got %d", wanted, actual)
	}

	grid = grid.fold(Fold{'x', 5})
	//grid.Print()

	actual = len(grid.points)
	if wanted := 16; wanted != actual {
		t.Errorf("Wanted %d, got %d", wanted, actual)
	}
}
