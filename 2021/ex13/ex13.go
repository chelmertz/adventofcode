package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

type Point struct {
	x, y int
}

func (p Point) String() string {
	return fmt.Sprintf("%d-%d", p.x, p.y)
}

type PointSet = map[string]Point

type Fold struct {
	direction rune
	number    int
}

type Grid struct {
	points     PointSet
	folds      []Fold
	maxY, maxX int
}

func (g *Grid) Print() {
	for y := 0; y < g.maxY+1; y++ {
		for x := 0; x < g.maxX+1; x++ {
			if _, found := g.points[Point{x, y}.String()]; found {
				fmt.Print("#")
			} else {
				fmt.Print(".")
			}
		}
		fmt.Println()
	}
	fmt.Println()
}

func (g *Grid) fold(fold Fold) (newGrid *Grid) {
	newGrid = &Grid{}
	newPoints := make(PointSet)
	maxX, maxY := g.maxX, g.maxY

	switch fold.direction {
	case 'x':
		maxX = fold.number - 1
		for _, p := range g.points {
			if p.x == fold.number {
				// on the fold, gone
				continue
			}

			if p.x > fold.number {
				p = Point{2*fold.number - p.x, p.y}
			} else {
				// keep as is
			}

			newPoints[p.String()] = p
		}
	case 'y':
		maxY = fold.number - 1
		for _, p := range g.points {
			if p.y == fold.number {
				// on the fold, gone
				continue
			}
			if p.y > fold.number {
				p = Point{p.x, 2*fold.number - p.y}
			} else {
				// keep as is
			}

			newPoints[p.String()] = p
		}
	default:
		panic(fmt.Sprintf("weird folding, should be x or y, got '%c'", fold.direction))
	}

	newGrid.points = newPoints
	newGrid.maxX = maxX
	newGrid.maxY = maxY

	return
}

func NewGrid(reader io.Reader) *Grid {
	scanner := bufio.NewScanner(reader)
	grid := &Grid{}
	grid.points = make(PointSet)
	grid.folds = make([]Fold, 0)

	readingCoords, readingFolds := true, false

	for scanner.Scan() {
		line := scanner.Text()
		if line == "" {
			readingCoords, readingFolds = false, true
			continue
		}
		if readingCoords {
			parts := strings.Split(line, ",")
			point := Point{}
			point.x, _ = strconv.Atoi(parts[0])
			point.y, _ = strconv.Atoi(parts[1])
			if point.x > grid.maxX {
				grid.maxX = point.x
			}
			if point.y > grid.maxY {
				grid.maxY = point.y
			}
			grid.points[point.String()] = point
		} else if readingFolds {
			foldAlong := strings.Split(line, "=")
			direction := foldAlong[0][len(foldAlong[0])-1]
			number, _ := strconv.Atoi(foldAlong[1])
			grid.folds = append(grid.folds, Fold{rune(direction), number})
		}
	}

	return grid
}

func Solve13(reader io.Reader) (a int) {
	grid := NewGrid(reader)
	a = -1
	for _, fold := range grid.folds {
		grid = grid.fold(fold)
		if a == -1 {
			a = len(grid.points)
		}
	}

	grid.Print()

	return
}

func main() {
	a := Solve13(os.Stdin)
	fmt.Println(a)
}
