package main

import (
	"bufio"
	"container/list"
	"fmt"
	"io"
	"os"
	"sort"
	"strconv"
	"strings"
)

func NumOfString(s string) int {
	number, err := strconv.Atoi(s)
	if err != nil {
		panic(err)
	}
	return number
}

type Point struct {
	x, y int
}

func (p Point) Hash() string {
	return fmt.Sprintf("%d %d", p.x, p.y)
}

func OfHash(hash string) Point {
	var x, y int
	for i, asString := range strings.Split(hash, " ") {
		asNumber, err := strconv.Atoi(asString)
		if err != nil {
			panic(err)
		}
		if i == 0 {
			x = asNumber
		} else {
			y = asNumber
		}
	}
	return Point{x, y}
}

func (p Point) String() string {
	return fmt.Sprintf("Point{x:%d; y:%d}", p.x, p.y)
}

type GridLine map[int]int
type Grid struct {
	lines         map[int]GridLine
	height, width int
}

// TODO replace xy pair with Point all over (why do I never start with Point?)
func (g *Grid) LowerThanNeighbours(x, y int) bool {
	curr := g.lines[y][x]

	// up
	if y > 0 && g.lines[y-1][x] <= curr {
		return false
	}

	// down
	if y < g.height-1 && g.lines[y+1][x] <= curr {
		return false
	}

	// left
	if x > 0 && g.lines[y][x-1] <= curr {
		return false
	}

	// right
	if x < g.width-1 && g.lines[y][x+1] <= curr {
		return false
	}

	return true
}

func (g *Grid) WithinBounds(point Point) bool {
	if point.x < 0 || point.x > g.width-1 {
		return false
	}
	if point.y < 0 || point.y > g.height-1 {
		return false
	}
	return true
}

func (g *Grid) ValueAt(x, y int) int {
	return g.lines[y][x]
}

func (g *Grid) ValueOf(point Point) int {
	return g.lines[point.y][point.x]
}

func (g *Grid) Surrounding(point Point, shouldVisit func(newPoint Point) bool) (directionsMatchingFilter []Point) {
	directionDeltas := [4][2]int{
		// left
		{-1, 0},
		// right
		{1, 0},
		// up
		{0, 1},
		// down
		{0, -1},
	}

	directionsMatchingFilter = make([]Point, 0)

	for _, delta := range directionDeltas {
		newPoint := Point{point.x + delta[0], point.y + delta[1]}
		//fmt.Printf("starting with %v (value %d), looking at (value %d), ok: %t\n", point, g.ValueOf(point), g.ValueOf(newPoint), shouldVisit(newPoint))
		if shouldVisit(newPoint) {
			directionsMatchingFilter = append(directionsMatchingFilter, newPoint)
		}
	}

	return
}

// The string key is from Point.Hash(), turn into point again with Point.ofHash()
type Basin struct {
	coords   map[string]bool
	lowpoint Point
}

func (g *Grid) NewBasin(startingPoint Point) *Basin {
	basin := &Basin{
		coords:   make(map[string]bool),
		lowpoint: startingPoint,
	}

	// list of Point
	queue := list.New()
	queue.PushFront(startingPoint)

	for curr := queue.Front(); curr != nil; curr = curr.Next() {
		point := curr.Value.(Point)
		currValue := g.ValueOf(point)
		basin.coords[point.Hash()] = true

		interesting := g.Surrounding(point, func(newPoint Point) bool {
			if _, alreadyVisited := basin.coords[newPoint.Hash()]; alreadyVisited {
				return false
			}
			newValue := g.ValueOf(newPoint)
			return newValue != 9 && newValue > currValue
		})
		if startingPoint.x == 2 && startingPoint.y == 2 {
			fmt.Printf("Looking at %v with value %d, will look at %v\n", point, g.ValueOf(point), interesting)
		}
		for _, surrounding := range interesting {
			queue.PushBack(surrounding)
		}
	}

	return basin
}

func NewGridOfReader(reader io.Reader) *Grid {
	scanner := bufio.NewScanner(reader)

	// [y][x], we're reading linewise
	grid := &Grid{
		lines: make(map[int]GridLine),
	}

	var y int
	for scanner.Scan() {
		line := scanner.Text()

		if grid.width == 0 {
			grid.width = len(line)
		}

		grid.lines[y] = make(GridLine)
		for x, s := range strings.Split(line, "") {
			grid.width = len(line)

			number := NumOfString(s)
			grid.lines[y][x] = number
		}

		y++
	}

	grid.height = y

	return grid
}

func Solve9A(reader io.Reader) (int, error) {
	grid := NewGridOfReader(reader)

	lowpointSum := 0

	for y := 0; y < grid.height; y++ {
		for x := 0; x < grid.width; x++ {
			if grid.LowerThanNeighbours(x, y) {
				//fmt.Printf("Got a lowpoint at [y=%d; x=%d]: %d\n", y, x, grid.ValueAt(x, y))
				lowpointSum += grid.ValueAt(x, y) + 1
			}
		}
	}

	return lowpointSum, nil
}

// https://en.wikipedia.org/wiki/ANSI_escape_code
// mentions that 0x1B is the start of an escape sequence,
// and in golang, \xAB is the hexadecimal notation of exactly two digits
var ascii_yellow = "\x1b[33m"
var ascii_red = "\x1b[31m"
var ascii_reset = "\x1b[39;49m"

func (g *Grid) PrintBasin(basin *Basin) {
	lpHash := basin.lowpoint.Hash()
	for y := 0; y < g.height; y++ {
		for x := 0; x < g.width; x++ {
			hash := fmt.Sprintf("%d %d", x, y)
			// ridicuously many transformations
			if lpHash == hash {
				fmt.Printf("%s%d%s", ascii_yellow, g.ValueAt(x, y), ascii_reset)
			} else if _, found := basin.coords[hash]; found {
				fmt.Printf("%s%d%s", ascii_red, g.ValueAt(x, y), ascii_reset)
			} else {
				fmt.Print(g.ValueAt(x, y))
			}
		}
		fmt.Println()
	}
	fmt.Println()
}

func Solve9B(reader io.Reader) (int, error) {
	grid := NewGridOfReader(reader)

	basins := make([]*Basin, 0)
	lowpoints := make([]Point, 0)

	// TODO extract to grid.Lowpoints()
	for y := 0; y < grid.height; y++ {
		for x := 0; x < grid.width; x++ {
			if grid.LowerThanNeighbours(x, y) {
				//fmt.Printf("Got a lowpoint at [y=%d; x=%d]: %d\n", y, x, grid.ValueAt(x, y))
				lowpoints = append(lowpoints, Point{x, y})
			}
		}
	}

	// TODO make this parallel
	for _, low := range lowpoints {
		basin := grid.NewBasin(low)
		//fmt.Printf("low point %v (value %d) has size %d\n", low, grid.ValueOf(low), len(basin))
		basins = append(basins, basin)
	}

	sort.Slice(basins, func(i, j int) bool {
		return len(basins[i].coords) < len(basins[j].coords)
	})

	sum := 1
	for _, basin := range basins[len(basins)-3:] {
		grid.PrintBasin(basin)
		sum *= len(basin.coords)
	}

	// 1241460 == too high
	// 515780 == too low
	return sum, nil
}

func main() {
	res, err := Solve9B(os.Stdin)

	if err != nil {
		panic(nil)
	}

	fmt.Printf("sum: %d\n", res)
}
