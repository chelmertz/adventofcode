package main

import "testing"
import "strings"

func TestLowerThanNeighbours_AtEdges(t *testing.T) {
	grid := NewGridOfReader(strings.NewReader("10\n01"))
	testCases := []struct {
		y, x   int
		wanted bool
	}{
		{0, 0, false},
		{0, 1, true},
		{1, 0, true},
		{1, 1, false},
	}

	for _, tc := range testCases {
		if actual := grid.LowerThanNeighbours(tc.x, tc.y); actual != tc.wanted {
			t.Errorf("Wanted %t but got %t for [y: %d, x: %d] in grid %v", tc.wanted, actual, tc.y, tc.x, grid)
		}
	}

}

func TestSolve9A(t *testing.T) {
	res, err := Solve9A(strings.NewReader(`2199943210
3987894921
9856789892
8767896789
9899965678`))

	if err != nil {
		t.Errorf("Got error %v", err)
	}

	if wanted := 15; wanted != res {
		t.Errorf("Wanted %d, got %d", wanted, res)
	}
}

func TestSolve9B(t *testing.T) {
	res, err := Solve9B(strings.NewReader(`2199943210
3987894921
9856789892
8767896789
9899965678`))

	if err != nil {
		t.Errorf("Got error %v", err)
	}

	if wanted := 1134; wanted != res {
		t.Errorf("Wanted %d, got %d", wanted, res)
	}
}
