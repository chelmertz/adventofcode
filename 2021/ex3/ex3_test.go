package main

import (
	"strings"
	"testing"
)

func TestA(t *testing.T) {
	result, err := Solve3a(strings.NewReader(`00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010`))

	if err != nil {
		t.Errorf("%v", err)
	}

	if wanted := 198; result != wanted {
		t.Errorf("Wrong result, wanted %d, got %d", wanted, result)
	}
}

func TestB(t *testing.T) {
	result, err := Solve3b(strings.NewReader(`00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010`))

	if err != nil {
		t.Errorf("%v", err)
	}

	if wanted := 230; result != wanted {
		t.Errorf("Wrong result, wanted %d, got %d", wanted, result)
	}
}
