package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
)

func Solve3a(reader io.Reader) (int, error) {
	scanner := bufio.NewScanner(reader)

	lines := 0
	numbers := -1
	var result []int
	for scanner.Scan() {
		line := scanner.Text()
		if numbers < 0 {
			numbers = len(line)
			result = make([]int, numbers, numbers)
		}
		fmt.Printf("line: %s, numbers: %d, result: %v\n", line, numbers, cap(result))
		for i := 0; i < numbers; i++ {
			if line[i] == '1' {
				result[i]++
			}
		}
		lines++
	}

	var gamma, epsilon int
	fmt.Printf("numbers before second loop: %d\n", numbers)
	for i := 0; i < numbers; i++ {
		// result is highest bit first
		if result[i] > lines/2.0 {
			fmt.Printf("gamma: %d, %v\n", i, result[i])
			gamma |= 1 << (numbers - i - 1)
		} else {
			fmt.Printf("epsilon: %d, %v\n", i, result[i])
			// or: epsilon = ~gamma (ish, dunno the correct syntax)
			epsilon |= 1 << (numbers - i - 1)
		}
	}

	product := gamma * epsilon
	fmt.Printf("result: %v\n", result)
	fmt.Printf("gamma: %d, epsi: %d, product: %d\n", gamma, epsilon, product)
	return product, nil
}

func countBitsForIndex(lines []string, index int) (mostCommon byte, isTie bool) {
	var ones float64
	halfLength := float64(len(lines)) / 2.0
	for _, line := range lines {
		if line[index] == '1' {
			ones++
		}
	}
	if halfLength == ones {
		return '1', true
	} else if halfLength > ones {
		return '0', false
	}
	return '1', false
}

type determineContinueIfByte func(mostCommon byte, isTie bool) byte

func findStuff(lines []string, continueIf determineContinueIfByte) int {
	var continueWith []string
	ourCopy := make([]string, len(lines))
	copy(ourCopy, lines)
	for index := 0; index < len(lines); index++ {
		continueWith = nil
		mostCommon, isTie := countBitsForIndex(ourCopy, index)
		bitIsValid := continueIf(mostCommon, isTie)
		for _, line := range ourCopy {
			if line[index] == bitIsValid {
				continueWith = append(continueWith, line)
			}
		}

		if len(continueWith) > len(ourCopy) {
			log.Fatalf("continue with (%d) should be smaller or of equal length to lines (%d)", len(continueWith), len(ourCopy))
		}

		if len(continueWith) == 1 {
			found, err := strconv.ParseInt(continueWith[0], 2, 64)
			if err != nil {
				log.Fatalf("Could not translate '%s' to an int", continueWith[0])
			}

			return int(found)
		}

		ourCopy = continueWith
	}
	panic("no single result found")
}

func Solve3b(reader io.Reader) (int, error) {
	scanner := bufio.NewScanner(reader)

	var lines []string
	lineLength := -1
	for scanner.Scan() {
		line := scanner.Text()
		lines = append(lines, line)
		if lineLength < 0 {
			lineLength = len(line)
		}
	}

	oxygen := findStuff(lines, func(mostCommon byte, isTie bool) byte {
		if isTie {
			return '1'
		}
		return mostCommon
	})
	co2 := findStuff(lines, func(mostCommon byte, isTie bool) byte {
		if isTie {
			return '0'
		}
		if mostCommon == '1' {
			return '0'
		}
		return '1'
	})

	product := oxygen * co2
	return product, nil
}

func main() {
	result, err := Solve3b(os.Stdin)
	if err != nil {
		panic(err)
	}

	fmt.Println(result)
}
