package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
)

type PathSet map[string]struct{}
type PathCount map[string]int

type Edges map[string][]string

type Graph struct {
	edges Edges
}

var a, z = int('a'), int('z')

func IsLowerCase(letters string) bool {
	for _, letter := range letters {
		l := int(letter)
		if l < a || l > z {
			return false
		}
	}
	return true
}

func copyPathSet(original PathSet) (copy PathSet) {
	copy = make(PathSet)
	for k, v := range original {
		copy[k] = v
	}
	return
}

func copyPathCount(original PathCount) (copy PathCount) {
	copy = make(PathCount)
	for k, v := range original {
		copy[k] = v
	}
	return
}

func (g *Graph) RecurseInto(prefix []string, excluded, paths PathSet) {
	head := prefix[len(prefix)-1]

	for _, to := range g.edges[head] {
		_, isVisited := excluded[to]
		if to == "end" {
			// TODO  could be a channel?
			paths[strings.Join(append(prefix, to), ",")] = struct{}{}
		} else if !isVisited {
			if IsLowerCase(to) {
				// only modify the rest of this path
				newExcluded := copyPathSet(excluded)
				newExcluded[to] = struct{}{}
				g.RecurseInto(append(prefix, to), newExcluded, paths)
			} else {
				g.RecurseInto(append(prefix, to), excluded, paths)
			}
		}
	}
	return
}

func CanVisitAnotherSmallCave(to string, smallCavesVisited PathCount) bool {
	if smallCavesVisited[to] == 0 {
		return true
	}

	for _, count := range smallCavesVisited {
		if count > 1 {
			return false
		}
	}

	return true
}

func (g *Graph) RecurseIntoB(prefix []string, smallCavesVisited PathCount, paths PathSet) {
	head := prefix[len(prefix)-1]

	for _, to := range g.edges[head] {
		isLowerCase := IsLowerCase(to)
		if to == "start" {
			// cannot return to "start" again
		} else if to == "end" {
			paths[strings.Join(append(prefix, to), ",")] = struct{}{}
		} else if isLowerCase {
			//fmt.Println("small caves", smallCavesVisited, to, CanVisitAnotherSmallCave(to, smallCavesVisited))
			if CanVisitAnotherSmallCave(to, smallCavesVisited) {
				// only modify the rest of this path
				newSmallCavesVisited := copyPathCount(smallCavesVisited)
				newSmallCavesVisited[to]++
				g.RecurseIntoB(append(prefix, to), newSmallCavesVisited, paths)
			}
		} else {
			g.RecurseIntoB(append(prefix, to), smallCavesVisited, paths)
		}
	}
	return
}

func (g *Graph) PossiblePathsWithSmallCavesAtMostOnce() PathSet {
	paths := make(PathSet)
	lowerCaseVisited := make(PathSet)

	g.RecurseInto([]string{"start"}, lowerCaseVisited, paths)

	return paths
}

func (g *Graph) PossiblePathsForB() PathSet {
	paths := make(PathSet)
	smallCavesVisited := make(PathCount)

	g.RecurseIntoB([]string{"start"}, smallCavesVisited, paths)

	return paths
}

func Solve12(reader io.Reader) (a, b int) {
	graph := &Graph{}
	graph.edges = make(Edges)
	scanner := bufio.NewScanner(reader)
	for scanner.Scan() {
		line := scanner.Text()
		parts := strings.Split(line, "-")

		if parts[1] == "start" || parts[0] == "end" {
			parts[0], parts[1] = parts[1], parts[0]
		}

		graph.edges[parts[0]] = append(graph.edges[parts[0]], parts[1])

		// double edged
		if parts[0] != "start" && parts[1] != "end" {
			graph.edges[parts[1]] = append(graph.edges[parts[1]], parts[0])
		}
	}

	a = len(graph.PossiblePathsWithSmallCavesAtMostOnce())
	b = len(graph.PossiblePathsForB())

	return
}

func main() {
	a, b := Solve12(os.Stdin)
	fmt.Println(a, b)
}
