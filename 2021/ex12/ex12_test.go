package main

import (
	"strings"
	"testing"
)

func TestSolveSmall(t *testing.T) {
	a, _ := Solve12(strings.NewReader(`start-A
start-b
A-c
A-b
b-d
A-end
b-end`))

	if wanted := 10; wanted != a {
		t.Errorf("Wanted %d, got %d", wanted, a)
	}
}

func TestSolveLarger(t *testing.T) {
	a, _ := Solve12(strings.NewReader(`dc-end
HN-start
start-kj
dc-start
dc-HN
LN-dc
HN-end
kj-sa
kj-HN
kj-dc`))

	if wanted := 19; wanted != a {
		t.Errorf("Wanted %d, got %d", wanted, a)
	}
}

func TestSolveEvenLarger(t *testing.T) {
	a, _ := Solve12(strings.NewReader(`fs-end
he-DX
fs-he
start-DX
pj-DX
end-zg
zg-sl
zg-pj
pj-he
RW-he
fs-DX
pj-RW
zg-RW
start-pj
he-WI
zg-he
pj-fs
start-RW`))

	if wanted := 226; wanted != a {
		t.Errorf("Wanted %d, got %d", wanted, a)
	}
}

func TestSolveBLarger(t *testing.T) {
	_, b := Solve12(strings.NewReader(`dc-end
HN-start
start-kj
dc-start
dc-HN
LN-dc
HN-end
kj-sa
kj-HN
kj-dc`))

	if wanted := 103; wanted != b {
		t.Errorf("Wanted %d, got %d", wanted, b)
	}
}

func TestSolveBEvenLarger(t *testing.T) {
	_, b := Solve12(strings.NewReader(`fs-end
he-DX
fs-he
start-DX
pj-DX
end-zg
zg-sl
zg-pj
pj-he
RW-he
fs-DX
pj-RW
zg-RW
start-pj
he-WI
zg-he
pj-fs
start-RW`))

	if wanted := 3509; wanted != b {
		t.Errorf("Wanted %d, got %d", wanted, b)
	}
}

func TestLowercase(t *testing.T) {
	inWanted := map[string]bool{
		"A":  false,
		"AB": false,
		"a":  true,
		"aA": false,
	}

	for input, wanted := range inWanted {
		if actual := IsLowerCase(input); actual != wanted {
			t.Errorf("Wanted %t, got %t for %s", wanted, actual, input)
		}
	}
}
