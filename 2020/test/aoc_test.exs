defmodule AocTest do
  use ExUnit.Case
  doctest Aoc
  doctest Util

  test "pattern match map" do
    numb =
      case %{"num" => "152", "unit" => "cm"} do
        %{"num" => num, "unit" => "cm"} -> num
      end

    assert numb == "152"
  end
end
