defmodule Util do
  def load(year, day, format) do
    File.read!("input/#{year}_#{day}.txt")
    |> format_input(format)
  end

  @doc ~S"""
      iex> Util.format_input("a\n\nb\nc", :lines)
      ["a", "b", "c"]

      iex> Util.format_input("1\n4\n\n3", :number_lines)
      [1, 4, 3]

      iex> Util.format_input("a\n\nb", :newline_separated)
      ["a", "b"]
      iex> Util.format_input("a b c\n\nd e\nf", :newline_separated)
      ["a b c", "d e f"]
  """
  def format_input(input, format) when is_binary(input) do
    case format do
      :lines ->
        input |> lines()

      :number_lines ->
        input |> lines() |> Enum.map(&String.to_integer/1)

      :newline_separated ->
        input
        |> String.split("\n\n", trim: true)
        |> Enum.map(fn x -> x |> String.trim() end)

      :raw ->
        input
    end
  end

  def lines(file_contents) do
    file_contents |> String.split("\n", trim: true)
  end

  def answer(answer) do
    IO.inspect(answer, label: find_aoc_method())

    # short-circuit loops etc
    Process.exit(self(), :normal)
  end

  def log(thing) do
    IO.inspect(thing, pretty: true)
  end

  def log(thing, label) when is_binary(label) or is_atom(label) do
    IO.inspect(thing, label: label, pretty: true)
  end

  @doc ~S"""
      iex> Util.colon_keyvals_into_map("abc:123 def:456")
      %{"abc" => "123", "def" => "456"}
      iex> Util.colon_keyvals_into_map("abc:123\ndef:456")
      %{"abc" => "123", "def" => "456"}
  """
  def colon_keyvals_into_map(a_string) do
    a_string
    |> String.replace("\n", " ")
    |> String.split(" ", trim: true)
    |> Enum.map(fn x -> x |> String.split(":") |> List.to_tuple() end)
    |> Enum.reduce(%{}, fn {key, value}, acc -> Map.put(acc, key, value) end)
  end

  def str_match(x, regex) do
    case x do
      x when is_binary(x) -> String.match?(x, regex)
      _ -> false
    end
  end

  def in_list(x, list) do
    Enum.member?(list, x)
  end

  def bin_search([direction | []], min..max)
      when direction in [:lower, :higher] do
    case direction do
      :lower -> min
      :higher -> max
    end
  end

  @doc ~S"""
      iex> Util.bin_search([:lower, :higher, :lower, :higher, :higher, :lower, :lower], 0..127)
      44
      iex> Util.bin_search([:higher, :lower, :higher], 0..7)
      5
  """
  def bin_search([direction | tail], min..max)
      when direction in [:lower, :higher] do
    # +1 because we're 0 indexed
    diff = div(max + 1 - min, 2)

    next =
      case direction do
        :lower -> min..(max - diff)
        :higher -> (min + diff)..max
      end

    bin_search(tail, next)
  end

  @doc ~S"""
      iex> Util.find_gap([1, 2, 4])
      3
      iex> Util.find_gap([4, 2, 1])
      3
  """
  def find_gap(list) when is_list(list) do
    list
    |> Enum.sort()
    |> Enum.reduce_while(:start, fn current, acc ->
      cond do
        acc == :start -> {:cont, current}
        acc == current - 1 -> {:cont, current}
        true -> {:halt, current - 1}
      end
    end)
  end

  # TODO this works for answer(123) but not Enum.filter(..) |> length() |> answer()
  defp find_aoc_method() do
    Process.info(self(), :current_stacktrace)
    |> elem(1)
    |> find_aoc_method()
    |> case do
      {:error, _} -> "answer"
      {:ok, asdf} -> "answer for " <> asdf
    end
  end

  defp find_aoc_method([]), do: {:error, "could not find the calling aoc method"}

  defp find_aoc_method([head | tail]) do
    {module, method} = {elem(head, 0) |> Atom.to_string(), elem(head, 1) |> Atom.to_string()}

    cond do
      module == "Elixir.Aoc" && method =~ ~r/^aoc(\d+)/ -> {:ok, method}
      true -> find_aoc_method(tail)
    end
  end
end
