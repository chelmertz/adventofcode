defmodule Aoc do
  import Util

  def aoc1a do
    nums = load(2020, 1, :number_lines)
    for i <- nums, j <- nums, i + j == 2020, do: answer(i * j)
  end

  def aoc1b do
    nums = load(2020, 1, :number_lines)
    for i <- nums, j <- nums, k <- nums, i + j + k == 2020, do: answer(i * j * k)
  end

  @aoc2 ~r/^(?<first>\d+)-(?<second>\d+) (?<letter>[[:alpha:]]): (?<letters>[[:alpha:]]+)$/

  def aoc2a do
    load(2020, 2, :lines)
    |> Enum.filter(fn x ->
      matches = Regex.named_captures(@aoc2, x)

      min = String.to_integer(matches["first"])
      max = String.to_integer(matches["second"])

      count =
        matches["letters"]
        |> String.graphemes()
        |> Enum.count(fn x -> x == matches["letter"] end)

      count >= min && count <= max
    end)
    |> length()
    |> answer()
  end

  def aoc2b do
    load(2020, 2, :lines)
    |> Enum.filter(fn x ->
      matches = Regex.named_captures(@aoc2, x)

      first = String.to_integer(matches["first"])
      second = String.to_integer(matches["second"])

      find_at = fn x ->
        matches["letters"]
        |> String.graphemes()
        |> Enum.at(x - 1)
        |> (&==/2).(matches["letter"])
      end

      at_first = find_at.(first)
      at_second = find_at.(second)

      # ... xor?!
      (at_first && !at_second) || (!at_first && at_second)
    end)
    |> length()
    |> answer()
  end

  @aoc3_fixture """
  ..##.......
  #...#...#..
  .#....#..#.
  ..#.#...#.#
  .#...##..#.
  ..#.##.....
  .#.#.#....#
  .#........#
  #.##...#...
  #...##....#
  .#..#...#.#
  """

  def aoc3a_test do
    @aoc3_fixture
    |> lines()
    |> aoc3a()

    # should be 7
  end

  defp ao3_traverse(lines, {dx, dy}) do
    width = String.length(Enum.at(lines, 0))

    lines
    |> Enum.take_every(dy)
    |> Enum.reduce(
      {0, 0},
      fn current, acc ->
        {trees, x} = acc

        trees =
          case String.at(current, rem(x, width)) do
            "#" -> trees + 1
            "." -> trees
          end

        {trees, x + dx}
      end
    )
    |> elem(0)
  end

  def aoc3a, do: load(2020, 3, :lines) |> aoc3a()

  def aoc3a(lines) do
    lines
    |> ao3_traverse({3, 1})
    |> answer()

    # 145
  end

  def aoc3b_test do
    nexts = [
      {1, 1},
      {3, 1},
      {5, 1},
      {7, 1},
      {1, 2}
    ]

    slope =
      @aoc3_fixture
      |> lines()

    log(slope)

    nexts
    |> Enum.reduce(1, fn {dx, dy}, acc ->
      trees = ao3_traverse(slope, {dx, dy})

      acc *
        case trees do
          0 -> 1
          n -> n
        end
    end)
    |> answer()

    # should be 336
  end

  def aoc3b do
    nexts = [
      {1, 1},
      {3, 1},
      {5, 1},
      {7, 1},
      {1, 2}
    ]

    slope = load(2020, 3, :lines)

    # 7175203200 too high (not skipping every other line with y = 2)

    nexts
    |> Enum.reduce(1, fn {dx, dy}, acc ->
      trees = ao3_traverse(slope, {dx, dy})

      acc *
        case trees do
          0 -> 1
          n -> n
        end
    end)
    |> answer()

    # 3424528800
  end

  @aoc4_fixture """
  ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
  byr:1937 iyr:2017 cid:147 hgt:183cm

  iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
  hcl:#cfa07d byr:1929

  hcl:#ae17e1 iyr:2013
  eyr:2024
  ecl:brn pid:760753108 byr:1931
  hgt:179cm

  hcl:#cfa07d eyr:2025 pid:166559648
  iyr:2011 ecl:brn hgt:59in
  """
  @aoc4a_required MapSet.new([
                    "byr",
                    "iyr",
                    "eyr",
                    "hgt",
                    "hcl",
                    "ecl",
                    "pid"
                  ])

  def aoc4a_test do
    @aoc4_fixture
    |> String.split("\n\n", trim: true)
    |> Enum.filter(fn line ->
      parts =
        line
        |> String.split(["\n", " "])
        |> Enum.map(fn attr -> attr |> String.split(":") |> Enum.at(0) end)
        |> MapSet.new()
        |> MapSet.intersection(@aoc4a_required)
        |> MapSet.size()

      log(parts)
      parts >= 7
    end)
    |> Enum.count()
    |> answer()
  end

  def aoc4a do
    load(2020, 4, :newline_separated)
    |> Enum.filter(fn line ->
      parts =
        line
        |> Enum.map(fn attr -> attr |> String.split(":") |> Enum.at(0) end)
        |> MapSet.new()
        |> MapSet.intersection(@aoc4a_required)
        |> MapSet.size()

      log(parts)
      parts >= 7
    end)
    |> Enum.count()
    |> answer()

    # 222 too high (OK, one key too many in the required set)
  end

  # TODO put in Util
  def aoc4_between(x, min, max) do
    try do
      x
      |> String.to_integer()
      |> case do
        y when y >= min and y <= max -> true
        _ -> false
      end
    rescue
      _ -> false
    end
  end

  defp aoc4_hgt(value) do
    case value do
      x when is_binary(x) ->
        Regex.named_captures(~r/^(?<num>\d{2,3})(?<unit>(cm)|(in))$/, x)
        |> case do
          nil -> false
          %{"num" => num, "unit" => "cm"} -> aoc4_between(num, 150, 193)
          %{"num" => num, "unit" => "in"} -> aoc4_between(num, 59, 76)
        end

      _ ->
        false
    end
  end

  # TODO try nimble_parsec
  defp aoc4_key_if_valid_value(keyvals) do
    # TODO can we pipe this into &Kernel.>=/2 ?

    count =
      [
        #    byr (Birth Year) - four digits; at least 1920 and at most 2002.
        keyvals["byr"] |> aoc4_between(1920, 2002),

        #    iyr (Issue Year) - four digits; at least 2010 and at most 2020.
        keyvals["iyr"] |> aoc4_between(2010, 2020),

        #    eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
        keyvals["eyr"] |> aoc4_between(2020, 2030),

        #    hgt (Height) - a number followed by either cm or in:
        #        If cm, the number must be at least 150 and at most 193.
        #        If in, the number must be at least 59 and at most 76.
        keyvals["hgt"] |> aoc4_hgt(),

        #    hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
        keyvals["hcl"] |> str_match(~r/^#[a-f0-9]{6}$/),

        #    ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
        keyvals["ecl"] |> in_list("amb blu brn gry grn hzl oth" |> String.split(" ")),

        #    pid (Passport ID) - a nine-digit number, including leading zeroes.
        keyvals["pid"] |> str_match(~r/^\d{9}$/)
      ]
      |> Enum.filter(fn x -> x != false end)
      |> Enum.count()

    count >= 7
  end

  def aoc4b do
    load(2020, 4, :newline_separated)
    |> Enum.map(&colon_keyvals_into_map/1)
    |> Enum.filter(&aoc4_key_if_valid_value/1)
    |> Enum.count()
    |> answer()

    # 123
  end

  @doc ~S"""
      iex> Aoc.aoc5_boarding_pass_to_row_column("FBFBBFFRLR")
      357
      iex> Aoc.aoc5_boarding_pass_to_row_column("BFFFBBFRRR")
      567
      iex> Aoc.aoc5_boarding_pass_to_row_column("FFFBBBFRRR")
      119
      iex> Aoc.aoc5_boarding_pass_to_row_column("BBFFBBFRLL")
      820
  """
  def aoc5_boarding_pass_to_row_column(x) when is_binary(x) do
    line = x |> String.graphemes()

    row =
      line
      |> Enum.slice(0..6)
      |> Enum.map(fn col_dir ->
        case col_dir do
          "F" -> :lower
          "B" -> :higher
        end
      end)
      |> bin_search(0..127)

    column =
      line
      |> Enum.slice(7..9)
      |> Enum.map(fn col_dir ->
        case col_dir do
          "L" -> :lower
          "R" -> :higher
        end
      end)
      |> bin_search(0..7)

    column + row * 8
  end

  def aoc5a do
    # I'm guessing 5b will be "move around people, upgrade someone to business
    # class", i.e. manipulate the tree.. or maybe inserting more seats. oh well,
    # brute forcing until then
    load(2020, 5, :lines)
    |> Enum.map(&aoc5_boarding_pass_to_row_column/1)
    |> Enum.max()
    |> answer()

    # 959
  end

  def aoc5b do
    # so I guess this is where you should have looked at the imbalanced tree in
    # "just a few steps" instead of iterating through it a bunch of times
    load(2020, 5, :lines)
    |> Enum.map(&aoc5_boarding_pass_to_row_column/1)
    |> find_gap()
    |> answer()

    # 527
  end

  @aoc6 ~S"""
  abc

  a
  b
  c

  ab
  ac

  a
  a
  a
  a

  b
  """

  def aoc6a_test do
    @aoc6
    |> format_input(:newline_separated)
    |> aoc6a()
    |> answer()

    # 11
  end

  def aoc6a(input) do
    input
    |> Enum.map(fn x ->
      x
      |> String.split("")
      |> Enum.map(&String.trim/1)
      |> Enum.filter(fn x -> x != "" end)
      |> MapSet.new()
      |> Enum.count()
    end)
    |> Enum.sum()

    # 6768
  end

  def aoc6a do
    load(2020, 6, :newline_separated) |> aoc6a() |> answer()
  end

  def aoc6b_test do
    @aoc6 |> log(:before_solving) |> String.split("\n") |> aoc6b() |> answer()

    # 6
  end

  def letter_set(string) when is_binary(string) do
    string |> String.graphemes() |> MapSet.new()
  end

  def aoc6b(lines) do
    lines
    |> Enum.reduce({MapSet.new(), :start_at_will, 0}, fn current, {ongoing, status, counter} ->
      log({ongoing, status, counter})
      log(current, "current")

      cond do
        status == :start_at_will && Enum.count(ongoing) == 0 ->
          {letter_set(current), :going, counter}

        status == :going and String.length(current) > 0 ->
          inter = MapSet.intersection(ongoing, current |> letter_set())

          case Enum.count(inter) do
            0 -> {MapSet.new(), :dead, counter}
            _ -> {inter, :going, counter}
          end

        String.length(current) == 0 ->
          {MapSet.new(), :start_at_will, counter + Enum.count(ongoing)}

        # we're just iterating through filled lines, waiting for a newline
        status == :dead && String.length(current) > 0 ->
          {MapSet.new(), :dead, counter}
      end
    end)
  end

  def aoc6b do
    # TODO raw -> lines, lines -> trimmed_lines
    # TODO rerun all previous days, I might have borked load()
    load(2020, 6, :raw) |> String.split("\n") |> aoc6b() |> answer()

    # 3489
  end
end
