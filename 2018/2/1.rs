use std::fs::File;
use std::collections::HashMap;
use std::io::Read;

#[derive(Debug)]
struct Checksum {
    twos: i32,
    threes: i32,
}

fn main() -> std::io::Result<()> {
    let mut contents = String::new();
    let mut checksum = Checksum {
        twos: 0,
        threes: 0,
    };

    File::open("1.txt")?.read_to_string(&mut contents)?;
    for line in contents.lines() {
        let mut counter = HashMap::new();
        for char in line.trim().chars() {
            let count = counter.entry(char).or_insert(0);
            *count += 1;
        }
        let mut twos = 0;
        let mut threes = 0;
        for (_key, value) in &counter {
            if *value == 2 {
                twos = 1;
            }
            if *value == 3 {
                threes = 1;
            }
            if twos == 1 && threes == 1 {
                break;
            }
        }

        if twos == 1 {
            checksum.twos = checksum.twos + 1;
        }
        if threes == 1 {
            checksum.threes = checksum.threes + 1;
        }
    }
    println!("{:?}, checksum: {}", checksum, checksum.twos * checksum.threes);

    Ok(())
}
