use std::fs::File;
use std::io::Read;
use std::collections::HashSet;

fn main() -> std::io::Result<()>{
    let mut contents = String::new();
    let mut result = 0;
    let mut results = HashSet::new();
    File::open("1.txt")?.read_to_string(&mut contents)?;
    for line in contents.lines().cycle() {
        let value: i32 = line.trim()[1..].parse().unwrap();
        match &line[0..1] {
            "+" => result += value,
            "-" => result -= value,
            _ => panic!("invalid input")
        }
        if results.contains(&result) {
            println!("{}", result);
            return Ok(());
        }
        results.insert(result);
    }
    println!("Found nothing");
    Ok(())
}
