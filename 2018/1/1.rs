use std::fs::File;
use std::io::Read;

fn main() -> std::io::Result<()>{
    let mut contents = String::new();
    let mut result = 0;
    File::open("1.txt")?.read_to_string(&mut contents)?;
    for line in contents.lines() {
        let value: i32 = line.trim()[1..].parse().unwrap();
        match &line[0..1] {
            "+" => result += value,
            "-" => result -= value,
            _ => panic!("invalid input")
        }
    }
    println!("{}", result);
    Ok(())
}
