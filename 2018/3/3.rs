use std::fs::File;
use std::io::Read;
use std::collections::HashSet;

fn main() -> std::io::Result<()> {
    let mut contents = String::new();
    File::open("input.txt")?.read_to_string(&mut contents)?;
    let mut taken: HashSet<(i32, i32)> = HashSet::new();
    let mut clashes = 0;
    for line in contents.lines() {
        let parts: Vec<&str> = line.trim().split("@").collect();
        let second_part: Vec<&str> = parts[1].split(":").collect();
        let coords = second_part[0].trim().split(",").collect::<Vec<_>>();
        let size = second_part[1].trim().split("x").collect::<Vec<_>>();
        println!("coords: {:?}, size: {:?}", coords, size);
        break;
    }

    println!("Clashes: {}", clashes);
    Ok(())
}
