# to test this file, from vim:
# :!pytest %
# or
# :nnoremap <leader>t :!pytest %<cr>
# and then
# ,t

def blocks_away(x, y):
     return abs(x) + abs(y)

def visited_twice(already_visited, old_position, new_x=None, new_y=None):
    pass

def blocks(steps):
    direction = 0 # north, 1 = east, ...
    position = (0, 0) # x, y
    already_visited = set([position])

    for step in map(str.strip, steps.split(',')):
        letter, number = step[0], int(step[1:])
        if step[0] == 'R':
            if direction == 0:
                position = (position[0] + number, position[1])
                direction = 1
            elif direction == 1:
                position = (position[0], position[1] - number)
                direction = 2
            elif direction == 2:
                position = (position[0] - number, position[1])
                direction = 3
            elif direction == 3:
                position = (position[0], position[1] + number)
                direction = 0
        elif step[0] == 'L':
            if direction == 0:
                position = (position[0] - number, position[1])
                direction = 3
            elif direction == 1:
                position = (position[0], position[1] + number)
                direction = 0
            elif direction == 2:
                position = (position[0] + number, position[1])
                direction = 1
            elif direction == 3:
                position = (position[0], position[1] - number)
                direction = 2
    return (blocks_away(*position), visited_twice)

def test_helper_blocks_away():
    assert 1 == blocks_away(-1, 0)
    assert 2 == blocks_away(-1, 1)
    assert 0 == blocks_away(0, 0)

def test_helper_visited_twice():
    assert (4, 0) == visited_twice(set((4, 0)), (3, 0), new_x=4)

def test_example_one():
    assert 5 == blocks('R2, L3')[0]

def test_example_two():
    assert 2 == blocks('R2, R2, R2')[0]

def test_example_three():
    assert 12 == blocks('R5, L5, R5, R3')[0]

def test_input_part_one():
    assert 146 == blocks('R4, R4, L1, R3, L5, R2, R5, R1, L4, R3, L5, R2, L3, L4, L3, R1, R5, R1, L3, L1, R3, L1, R2, R2, L2, R5, L3, L4, R4, R4, R2, L4, L1, R5, L1, L4, R4, L1, R1, L2, R5, L2, L3, R2, R1, L194, R2, L4, R49, R1, R3, L5, L4, L1, R4, R2, R1, L5, R3, L5, L4, R4, R4, L2, L3, R78, L5, R4, R191, R4, R3, R1, L2, R1, R3, L1, R3, R4, R2, L2, R1, R4, L5, R2, L2, L4, L2, R1, R2, L3, R5, R2, L3, L3, R3, L1, L1, R5, L4, L4, L2, R5, R1, R4, L3, L5, L4, R5, L4, R5, R4, L3, L2, L5, R4, R3, L3, R1, L5, R5, R1, L3, R2, L5, R5, L3, R1, R4, L5, R4, R2, R3, L4, L5, R3, R4, L5, L5, R4, L4, L4, R1, R5, R3, L1, L4, L3, L4, R1, L5, L1, R2, R2, R4, R4, L5, R4, R1, L1, L1, L3, L5, L2, R4, L3, L5, L4, L1, R3')[0]

def test_can_we_visit_twice():
    assert (0, 0) == blocks('R2, R2, R2, R2')[1]

def test_part_two_example():
    assert (4, (4, 0)) == blocks('R8, R4, R4, R8')

def test_input_part_two():
    assert (184, (80, -104)) == blocks('R4, R4, L1, R3, L5, R2, R5, R1, L4, R3, L5, R2, L3, L4, L3, R1, R5, R1, L3, L1, R3, L1, R2, R2, L2, R5, L3, L4, R4, R4, R2, L4, L1, R5, L1, L4, R4, L1, R1, L2, R5, L2, L3, R2, R1, L194, R2, L4, R49, R1, R3, L5, L4, L1, R4, R2, R1, L5, R3, L5, L4, R4, R4, L2, L3, R78, L5, R4, R191, R4, R3, R1, L2, R1, R3, L1, R3, R4, R2, L2, R1, R4, L5, R2, L2, L4, L2, R1, R2, L3, R5, R2, L3, L3, R3, L1, L1, R5, L4, L4, L2, R5, R1, R4, L3, L5, L4, R5, L4, R5, R4, L3, L2, L5, R4, R3, L3, R1, L5, R5, R1, L3, R2, L5, R5, L3, R1, R4, L5, R4, R2, R3, L4, L5, R3, R4, L5, L5, R4, L4, L4, R1, R5, R3, L1, L4, L3, L4, R1, L5, L1, R2, R2, R4, R4, L5, R4, R1, L1, L1, L3, L5, L2, R4, L3, L5, L4, L1, R3')
