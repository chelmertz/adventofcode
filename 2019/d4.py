def check_higher(number):
    a, b, c, d, e, f = list(map(int, str(number)))
    return a <= b <= c <= d <= e <= f

def check_adj(number):
    a, b, c, d, e, f = list(map(int, str(number)))
    return a == b or b == c or c == d or d == e or e == f

def check_adj2(number):
    numbers = list(map(int, str(number)))
    #print(numbers)
    #print("===")
    last = None
    suite = 1
    for a in numbers:
        #print(a)
        #print(last)
        #print(suite)
        #print("===")
        if a == last:
            suite += 1
        elif suite == 2:
            return True
        else:
            suite = 1
        last = a
    if suite == 2:
        return True
    return False

def main(range_):
    min_, max_ = list(map(int, range_.split("-")))
    # let's guess inclusive
    matching = 0
    print(min_, max_)
    for curr in range(min_, max_ + 1):
        if check_higher(curr) and check_adj2(curr):
            matching += 1
    print(matching)

# p1: main("111111-111111") # 1
# p1: main("223450-223450") # 0
# p1: main("123789-123789") # 0
main("112233-112233") # 1
main("123444-123444") # 0
main("111122-111122") #1
main("356261-846303") # 159 is too low, 263 too low
