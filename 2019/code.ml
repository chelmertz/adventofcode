open Core

(* M-x merlin-use RET core *)

let load_day (day: int): string list =
  In_channel.read_lines @@ string_of_int day ^ ".txt"

let dbg_int (actual: int) (expected: int): unit =
  if actual = expected then
    ()
  else
    begin
      actual
      |> string_of_int
      |> (^) "debug: "
      |> Out_channel.print_endline;
      assert (actual = expected)
    end

let dbg_int_arr (actual: int array) (expected: int array): unit =
  if actual = expected then
    ()
  else
    begin
      Out_channel.print_endline "debug, actual: ";
      Base.Array.iter ~f:(printf "%d ") actual;
      Out_channel.newline stdout;
      Out_channel.print_endline "debug, expected: ";
      Base.Array.iter ~f:(printf "%d ") expected;
      Out_channel.newline stdout;
      assert (actual = expected)
    end

let d1_calc (mass: int): int =  (mass / 3) - 2

let d1_p1 (acc: int) (line: Base.string): int =
  acc + d1_calc (Int.of_string line)

let d1_p2 (acc: int) (line: Base.string): int =
  let rec fold_over (acc': int) (current: int) =
    if current > 0 then
      fold_over (acc' + current) (d1_calc current)
    else
      acc'
  in
  fold_over acc (d1_calc (Int.of_string line))


let day1 () =
  let day1 = In_channel.fold_lines (In_channel.create "1.txt") ~init:0 ~f:d1_p1 in
  Out_channel.print_endline @@ "d1p1: " ^ string_of_int day1;

  dbg_int (d1_p2 0 "14") 2;

  dbg_int (d1_p2 0 "1969") 966;

  let day2 = In_channel.fold_lines (In_channel.create "1.txt") ~init:0 ~f:d1_p2 in
  Out_channel.print_endline @@ "d2p2: " ^ string_of_int day2

type opcode_d2 = Stop | Add | Mult

let opcode_of_int_d2 = function
  | (1, _, _) -> Add
  | (2, _, _) -> Mult
  | (99, _, _) -> Stop
  | (b, pos, steps) -> failwith @@ "Bad opcode_d2 " ^ (string_of_int b) ^ " at position " ^ (string_of_int pos) ^ " after " ^ (string_of_int steps) ^ " steps"

type pointer = int
type state = Continue of pointer | Done

let apply_opcode_d2 (steps: int) (pointer: pointer) (inp: int array): state =
  let (opcode: opcode_d2) = opcode_of_int_d2 (inp.(pointer), pointer, steps) in
  match opcode with
  | Stop -> Done
  | Add ->
     let to_modify = inp.(pointer + 3) in
     let a = inp.(pointer + 1) in
     let b = inp.(pointer + 2) in
     let new_value = inp.(a) + inp.(b) in
     inp.(to_modify) <- new_value;
     Continue (pointer + 4)
  | Mult ->
     let to_modify = inp.(pointer + 3) in
     let a = inp.(pointer + 1) in
     let b = inp.(pointer + 2) in
     let new_value = inp.(a) * inp.(b) in
     inp.(to_modify) <- new_value;
     Continue (pointer + 4)

let int_array_of_comma_string (input: Base.string): pointer array =
  let split_on_comma = Str.split (Str.regexp ",") input in
  let numbers = List.map ~f:Int.of_string split_on_comma in
  Array.of_list numbers

let solve_day2 (input: Base.string): pointer array =
  let arr = int_array_of_comma_string input in
  let rec traverse steps pointer a =
    match apply_opcode_d2 steps pointer a with
    | Done -> a
    | Continue (new_pointer) -> traverse (steps + 1) new_pointer a
  in
  traverse 0 0 arr

let day2 () =
  dbg_int_arr (solve_day2 "1,0,0,0,99") [|2;0;0;0;99|];
  dbg_int_arr (solve_day2 "2,3,0,3,99") [|2;3;0;6;99|];
  dbg_int_arr (solve_day2 "2,4,4,5,99,0") [|2;4;4;5;99;9801|];
  dbg_int_arr (solve_day2 "1,1,1,4,99,5,6,0,99") [|30;1;1;4;2;5;6;0;99|];
  let real_inp = "1,12,2,3,1,1,2,3,1,3,4,3,1,5,0,3,2,10,1,19,2,19,6,23,2,13,23,27,1,9,27,31,2,31,9,35,1,6,35,39,2,10,39,43,1,5,43,47,1,5,47,51,2,51,6,55,2,10,55,59,1,59,9,63,2,13,63,67,1,10,67,71,1,71,5,75,1,75,6,79,1,10,79,83,1,5,83,87,1,5,87,91,2,91,6,95,2,6,95,99,2,10,99,103,1,103,5,107,1,2,107,111,1,6,111,0,99,2,14,0,0" in
  let result = (solve_day2 real_inp) in
  assert (result.(0) = 3716293)


type mode = Parameter | Immediate
type opcode = Stop | Add | Mult | Input | Output
type param_number = One | Two | Three

let opcode_of_int = function
  | 1 -> Add
  | 2 -> Mult
  | 3 -> Input
  | 4 -> Output
  | 99 -> Stop
  | bad -> failwith @@ "Bad opcode: " ^ (string_of_int bad)

let mode_of_int = function
  | 0 -> Parameter
  | 1 -> Immediate
  | bad -> failwith @@ "Bad mode: " ^ (string_of_int bad)

let value (instruction: int) (input: int array) (pointer: int) (parameter: param_number): int =
  let mode = mode_of_int @@ match parameter with
    | One -> (instruction / 100) mod 10
    | Two -> (instruction / 1000) mod 10
    | Three -> (instruction / 10000) mod 10
  in
  let index = pointer + (
      match parameter with
      | One -> 1
      | Two -> 2
      | Three -> 2
    )
  in
  match mode with
  | Parameter -> input.(input.(index))
  | Immediate -> input.(index)

exception Test_output of int

let solve_d5 (input: int array) =
  let rec solve_d5 (pointer: int) =
    let instruction = input.(pointer) in
    let value_of = value instruction input pointer in
    let opcode = opcode_of_int @@ instruction mod 100 in
    match opcode with
    | Add ->
       let a = value_of One in
       let b = value_of Two in
       let to_modify = value_of Three in
       let new_value = a + b in
       input.(to_modify) <- new_value;
       solve_d5 (pointer + 4)
    | Mult ->
       let a = value_of One in
       let b = value_of Two in
       let to_modify = value_of Three in
       let new_value = a * b in
       input.(to_modify) <- new_value;
       solve_d5 (pointer + 4)
    | Input ->
       (* guess work *)
       let to_change = input.(pointer + 1) in
       input.(to_change) <- 1;
       solve_d5 (pointer + 2)
    | Output ->
       (* guess work, TODO we might have to check the input and see if it's 0, in that case move on *)
       raise @@ Test_output input.(input.(pointer + 1))
    | Stop -> input
  in solve_d5 0

let day5 () =
  dbg_int_arr (solve_d5 (int_array_of_comma_string "1002,4,3,4,33")) [|1002;4;3;4;99|];
  begin
    try
      let _ = solve_d5 [|3;0;4;0;99|] in
      ()
    with Test_output (a) ->
          if a <> 1 then
            failwith @@ "Wanted 1, got " ^ (string_of_int a)
  end;
  let d5_input = "3,225,1,225,6,6,1100,1,238,225,104,0,1102,40,93,224,1001,224,-3720,224,4,224,102,8,223,223,101,3,224,224,1,224,223,223,1101,56,23,225,1102,64,78,225,1102,14,11,225,1101,84,27,225,1101,7,82,224,1001,224,-89,224,4,224,1002,223,8,223,1001,224,1,224,1,224,223,223,1,35,47,224,1001,224,-140,224,4,224,1002,223,8,223,101,5,224,224,1,224,223,223,1101,75,90,225,101,9,122,224,101,-72,224,224,4,224,1002,223,8,223,101,6,224,224,1,224,223,223,1102,36,63,225,1002,192,29,224,1001,224,-1218,224,4,224,1002,223,8,223,1001,224,7,224,1,223,224,223,102,31,218,224,101,-2046,224,224,4,224,102,8,223,223,101,4,224,224,1,224,223,223,1001,43,38,224,101,-52,224,224,4,224,1002,223,8,223,101,5,224,224,1,223,224,223,1102,33,42,225,2,95,40,224,101,-5850,224,224,4,224,1002,223,8,223,1001,224,7,224,1,224,223,223,1102,37,66,225,4,223,99,0,0,0,677,0,0,0,0,0,0,0,0,0,0,0,1105,0,99999,1105,227,247,1105,1,99999,1005,227,99999,1005,0,256,1105,1,99999,1106,227,99999,1106,0,265,1105,1,99999,1006,0,99999,1006,227,274,1105,1,99999,1105,1,280,1105,1,99999,1,225,225,225,1101,294,0,0,105,1,0,1105,1,99999,1106,0,300,1105,1,99999,1,225,225,225,1101,314,0,0,106,0,0,1105,1,99999,1007,226,677,224,1002,223,2,223,1005,224,329,1001,223,1,223,1007,226,226,224,1002,223,2,223,1006,224,344,101,1,223,223,1107,677,226,224,102,2,223,223,1006,224,359,1001,223,1,223,108,677,677,224,1002,223,2,223,1006,224,374,1001,223,1,223,107,677,677,224,1002,223,2,223,1005,224,389,101,1,223,223,8,677,677,224,1002,223,2,223,1005,224,404,1001,223,1,223,108,226,226,224,1002,223,2,223,1005,224,419,101,1,223,223,1008,677,677,224,1002,223,2,223,1005,224,434,101,1,223,223,1008,226,226,224,1002,223,2,223,1005,224,449,101,1,223,223,7,677,226,224,1002,223,2,223,1006,224,464,1001,223,1,223,7,226,226,224,1002,223,2,223,1005,224,479,1001,223,1,223,1007,677,677,224,102,2,223,223,1005,224,494,101,1,223,223,1108,677,226,224,102,2,223,223,1006,224,509,1001,223,1,223,8,677,226,224,102,2,223,223,1005,224,524,1001,223,1,223,1107,226,226,224,102,2,223,223,1006,224,539,1001,223,1,223,1008,226,677,224,1002,223,2,223,1006,224,554,1001,223,1,223,1107,226,677,224,1002,223,2,223,1006,224,569,1001,223,1,223,1108,677,677,224,102,2,223,223,1005,224,584,101,1,223,223,7,226,677,224,102,2,223,223,1006,224,599,1001,223,1,223,1108,226,677,224,102,2,223,223,1006,224,614,101,1,223,223,107,226,677,224,1002,223,2,223,1005,224,629,101,1,223,223,108,226,677,224,1002,223,2,223,1005,224,644,101,1,223,223,8,226,677,224,1002,223,2,223,1005,224,659,1001,223,1,223,107,226,226,224,1002,223,2,223,1006,224,674,101,1,223,223,4,223,99,226" in
  let _ = solve_d5 (int_array_of_comma_string d5_input) in
  ()


let () = day5 ()
