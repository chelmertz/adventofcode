with open("1.txt") as f:
    pos = 1
    floor = 0
    for c in f.readline():
        if c == '(':
            floor += 1
        else:
            floor -= 1
        if floor == -1:
            break
        pos += 1
    print(pos)
