let get_lines (filename: string): string list =
  let ic = open_in filename in
  let rec aux acc =
    try
      let line = input_line ic in
      aux ([line] @ acc)
    with End_of_file ->
      close_in_noerr ic;
      List.rev acc
  in aux []

let file_by_char (filename: string) (iter: (char -> unit)): unit =
  let ic = open_in filename in
  let rec aux () =
    try
      let char = input_char ic in
      iter char;
      aux ()
    with End_of_file ->
      close_in_noerr ic
  in aux ()

exception Done

let day1 () =
  let lines = get_lines "1.txt" in
  print_string ("lines: ");
  print_int @@ List.length lines;
  print_newline ();
  let sum_chars = ref 0 in
  file_by_char "1.txt" (fun _ -> sum_chars := !sum_chars + 1);
  let floor' = ref 0 in
  file_by_char "1.txt" (function
      | '(' -> floor' := !floor' + 1
      | ')' -> floor' := !floor' - 1
      | _ -> invalid_arg "weird input"
    );
  let pos' = ref 1 in
  let floor2' = ref 0 in
  try
    file_by_char "1.txt" (function
        | '(' -> floor2' := !floor2' + 1; pos' := !pos' + 1;
        | ')' -> floor2' := !floor2' - 1; if !floor2' = -1 then raise Done; pos' := !pos' + 1;
        | _ -> invalid_arg "weird input"
      );
  with Done -> ();
               print_endline @@ "chars: " ^ string_of_int !sum_chars;
               print_endline @@ "floor: " ^ string_of_int !floor';
               print_endline @@ "pos: " ^ string_of_int !pos'

let day2 () =
  let lines = get_lines "2.txt" in
  let formatted = List.map (fun line -> List.map int_of_string (String.split_on_char 'x' line)) lines in
  let calc sides =
    let [w; h; l] = List.sort compare sides in
    let surface_area = 2 * w * h + 2 * w * l + 2 * h * l in
    surface_area + w * h
  in
  let acc = (fun acc current -> acc + calc current) in
  let sqft = List.fold_left acc 0 formatted in
  print_endline @@ "sqft: " ^ string_of_int sqft;
  let ribbon sides =
    let [w; h; l] = List.sort compare sides in
    w * 2 + h * 2 + w * h * l
    in
  let acc_ribbon = (fun acc current -> acc + ribbon current) in
  let ft_ribbon = List.fold_left acc_ribbon 0 formatted in
  print_endline @@ "ribbon: " ^ string_of_int ft_ribbon

(* from https://caml.inria.fr/pub/docs/manual-ocaml/libref/Map.html *)
module IntPairs =
  struct
    type t = int * int
    let compare (x0,y0) (x1,y1) =
      match Pervasives.compare x0 x1 with
        0 -> Pervasives.compare y0 y1
      | c -> c
  end

module Grid = Map.Make(IntPairs)

let day3 () =
  let grid = Grid.(empty |> add (0, 0) 1) in
  let current_pos = ref (0, 0) in
  let traverse_grid dir =
    let (x, y) = !current_pos in
    match dir with
    | '^' -> current_pos := (x, y + 1)
    | '>' -> current_pos := (x + 1, y)
    | '<' -> current_pos := (x - 1, y)
    | 'v' -> current_pos := (x, y - 1);
             grid.ad


  file_by_char "3.txt" traverse_grid



  let () = day3 ()
